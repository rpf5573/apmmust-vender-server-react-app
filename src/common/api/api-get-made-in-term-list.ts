import axiosInstance from "@/axiosInstance";
import { API, Term } from "../types";
import { AxiosResponse } from "axios";
import { useQuery } from "@tanstack/react-query";

export type ApiGetMadeInTermList = API<unknown, Term[]>;

export const getMadeInTermList = async () => {
  const url = `${import.meta.env.VITE_API_URL}/term/get-made-in-term-list`;
  const response = await axiosInstance.get<
    ApiGetMadeInTermList["response"],
    AxiosResponse<ApiGetMadeInTermList["response"]>
  >(url, {
    timeout: 1000 * 3,
  });

  return response.data;
};

export const useGetMadeInTermList = () =>
  useQuery<ApiGetMadeInTermList["response"]>({
    queryKey: ["get-made-in-term-list"],
    queryFn: getMadeInTermList,
    staleTime: 1000 * 60 * 60 * 24, // 24시간 동안 캐싱
    gcTime: 1000 * 60 * 60 * 24, // 24시간 동안 캐싱
  });

export default useGetMadeInTermList;
