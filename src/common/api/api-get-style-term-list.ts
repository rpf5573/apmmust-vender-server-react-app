import axiosInstance from "@/axiosInstance";
import { API, Term } from "../types";
import { AxiosResponse } from "axios";
import { useQuery } from "@tanstack/react-query";

export type ApiGetStyleTermList = API<unknown, Term[]>;

export const getStyleTermList = async () => {
  const url = `${import.meta.env.VITE_API_URL}/term/get-style-term-list`;
  const response = await axiosInstance.get<
    ApiGetStyleTermList["response"],
    AxiosResponse<ApiGetStyleTermList["response"]>
  >(url, {
    timeout: 1000 * 3,
  });

  return response.data;
};

export const useGetStyleTermList = () =>
  useQuery<ApiGetStyleTermList["response"]>({
    queryKey: ["get-style-term-list"],
    queryFn: getStyleTermList,
    staleTime: 1000 * 60 * 60 * 24, // 24시간 동안 캐싱
    gcTime: 1000 * 60 * 60 * 24, // 24시간 동안 캐싱
  });

export default useGetStyleTermList;
