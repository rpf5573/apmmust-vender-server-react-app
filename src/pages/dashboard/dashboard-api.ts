import axiosInstance from "@/axiosInstance";
import { API, Dashboard } from "@/common/types";
import { useQuery } from "@tanstack/react-query";
import { AxiosResponse } from "axios";

export type ApiGetDashboard = API<unknown, Dashboard>;

// delete product
export const getDashboard = async () => {
  const url = `${import.meta.env.VITE_API_URL}/dashboard/get-dashboard`;
  const response = await axiosInstance.get<
    ApiGetDashboard["response"],
    AxiosResponse<ApiGetDashboard["response"]>
  >(url, {
    timeout: 10000,
  });

  return response.data;
};

export const useGetDashboard = () =>
  useQuery<ApiGetDashboard["response"]>({
    queryKey: ["get-dashboard"],
    queryFn: () => getDashboard(),
    staleTime: 1000 * 60 * 60 * 24, // 24시간 동안 캐싱. 주문 정보는 잘 변하지 않기 때문에 24시간으로 캐싱한다
    gcTime: 1000 * 60 * 60 * 24, // 24시간 동안 캐싱. 주문 정보는 잘 변하지 않기 때문에 24시간으로 캐싱한다
  });

export default useGetDashboard;
