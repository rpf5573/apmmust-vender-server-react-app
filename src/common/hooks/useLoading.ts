import { useSetAtom } from 'jotai';
import { useEffect } from 'react';

import { atom_showGlobalLoading } from '@pages/home/_HomePage';

const useLoading = (isLoading: boolean) => {
  const setLoading = useSetAtom(atom_showGlobalLoading);
  useEffect(() => {
    setLoading(isLoading);
  }, [isLoading]);

  return setLoading;
};

export default useLoading;
