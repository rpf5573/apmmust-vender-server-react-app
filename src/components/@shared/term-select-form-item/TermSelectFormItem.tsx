import { Term } from "@/common/types";
import { useEffect, useMemo, useState } from "react";
import { Controller, type UseFormReturn } from "react-hook-form";
import CustomFormItem from "../custom-form-item/CustomFormItem";
import { Button, CheckList, Popup, SearchBar, Space, Tag } from "antd-mobile";
import { findTermById, is초성match } from "@/common/utils";
import _, { isNull, isObject } from "underscore";
import { CheckCircleFill } from "antd-mobile-icons";

type TermSelectFormItemProps = {
  defaultTermList?: Term[];
  termList: Term[];
  isMultiple: boolean;
  fieldName: string;
  label: string;
  form: UseFormReturn;
  required?: boolean;
  errorMessage?: string;
  disabled?: boolean;
};

const TermSelectFormItem = ({
  defaultTermList,
  termList,
  isMultiple,
  fieldName,
  label,
  form,
  required,
  errorMessage,
  disabled,
}: TermSelectFormItemProps) => {
  const [selected, setSelected] = useState<string[]>(
    (defaultTermList ?? [])
      .filter(
        (item) => isObject(item) && !isNull(item) && _.has(item, "term_id")
      )
      .map((item) => `${item.term_id}`)
  );
  const [visible, setVisible] = useState(false);
  const [searchText, setSearchText] = useState("");

  const filteredTerms = useMemo(() => {
    if (!searchText) {
      return termList;
    }
    const filteredTerms = termList
      .filter((term) => {
        const termName = term.name.toLocaleLowerCase();
        const st = searchText.toLocaleLowerCase();
        return termName.includes(st) || is초성match(st, termName); // 초성 검색도 허용한다
      })
      // 초성끼리 붙어있는 단어를 먼저 보여준다
      .map((term) => {
        return term;
      });

    return filteredTerms;
  }, [termList, searchText]);

  // selected가 바뀌면 form에도 업데이트
  useEffect(() => {
    form.setValue(fieldName, isMultiple ? selected : selected[0]);
    if (!_.isEmpty(selected)) {
      form.clearErrors(fieldName);
    }
  }, [selected]);

  const handleStyleChange = (val: Array<string>) => {
    !isMultiple && setVisible(false);
    setSelected(val);
  };

  return (
    <Controller
      name={fieldName}
      control={form.control}
      render={({ fieldState: { invalid, error } }) => (
        <CustomFormItem
          label={label}
          required={required}
          showError={invalid}
          errorMessage={error?.message}
        >
          <Space align="center">
            <Button
              onClick={() => {
                setVisible(true);
              }}
              disabled={disabled}
            >
              선택
            </Button>
            <div className="flex gap-x-1">
              {selected.map((term_id) => (
                <Tag
                  key={term_id}
                  color="primary"
                  fill="outline"
                  className="!text-[16px]"
                >
                  {findTermById(termList, term_id)?.name}
                </Tag>
              ))}
            </div>
          </Space>
          <Popup
            className="form-item-content--size__popup"
            visible={visible}
            onMaskClick={() => {
              setVisible(false);
            }}
            destroyOnClose
            position="top"
          >
            <div className="search-bar-container p-[12px] mt-7">
              <SearchBar
                placeholder="검색"
                value={searchText}
                onChange={(v) => {
                  setSearchText(v);
                }}
              />
            </div>
            <div className="check-list-container">
              <CheckList
                multiple={isMultiple}
                defaultValue={[...selected]}
                onChange={handleStyleChange}
                extra={(active) => (active ? <CheckCircleFill /> : "")}
              >
                {filteredTerms.map((item) => (
                  <CheckList.Item key={item.term_id} value={`${item.term_id}`}>
                    {item.name}
                  </CheckList.Item>
                ))}
              </CheckList>
            </div>
          </Popup>
        </CustomFormItem>
      )}
      rules={{ required: errorMessage }}
    />
  );
};

export default TermSelectFormItem;
