import axiosInstance from "@/axiosInstance";
import { API } from "@/common/types";
import type { DefaultError } from "@tanstack/query-core";
import { useMutation } from "@tanstack/react-query";
import { AxiosResponse } from "axios";

export type ApiPostLogin = API<
  {
    email: string;
    password: string;
  },
  {
    token: string;
    user_display_name: string;
    user_email: string;
    user_nicename: string;
  }
>;

// login
export const postLogin = async ({
  email,
  password,
}: ApiPostLogin["params"]) => {
  const response = await axiosInstance.post<
    ApiPostLogin["response"],
    AxiosResponse<ApiPostLogin["response"]>,
    ApiPostLogin["variables"]
  >(
    `${import.meta.env.VITE_API_URL}/login`,
    { email, password },
    {
      timeout: 5000,
    }
  );

  return response.data;
};

export const usePostLogin = () =>
  useMutation<
    ApiPostLogin["response"],
    DefaultError,
    ApiPostLogin["variables"]
  >({
    mutationFn: postLogin,
  });

export default usePostLogin;
