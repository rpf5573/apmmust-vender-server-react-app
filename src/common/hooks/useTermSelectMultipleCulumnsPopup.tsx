import { Term } from "../types";
import { useMemo, useState } from "react";
import _, { isNull, isObject } from "underscore";
import { is초성match } from "../utils";
import { CheckList, Popup, SearchBar } from "antd-mobile";
import { CheckCircleFill } from "antd-mobile-icons";

type Props = {
  defaultTermList?: Term[];
  termList: Term[];
  isMultiple: boolean;
};
const useTermSelectMultipleColumnsPopup = ({
  defaultTermList,
  termList,
  isMultiple,
}: Props) => {
  const [selected, setSelected] = useState<string[]>(
    (defaultTermList ?? [])
      .filter(
        (item) => isObject(item) && !isNull(item) && _.has(item, "term_id")
      )
      .map((item) => `${item.term_id}`)
  );
  const [visible, setVisible] = useState(false);
  const [searchText, setSearchText] = useState("");

  const filteredTerms = useMemo(() => {
    if (!searchText) {
      return termList;
    }
    const filteredTerms = termList
      .filter((term) => {
        const termName = term.name.toLocaleLowerCase();
        const st = searchText.toLocaleLowerCase();
        return termName.split("_").some((name) => {
          return name.includes(st) || is초성match(st, name); // 초성 검색도 허용한다
        });
      })
      // 초성끼리 붙어있는 단어를 먼저 보여준다
      .map((term) => {
        return term;
      });

    return filteredTerms;
  }, [termList, searchText]);

  const [oneTerms, twoTerms, threeTerms] = filteredTerms.reduce(
    ([first, second, third], currentValue, currentIndex) => {
      switch (currentIndex % 3) {
        case 0:
          first.push(currentValue);
          break;
        case 1:
          second.push(currentValue);
          break;
        case 2:
          third.push(currentValue);
          break;
      }
      return [first, second, third];
    },
    [[], [], []] as [Term[], Term[], Term[]]
  );

  const handleTermListChange = (val: Array<string>) => {
    !isMultiple && setVisible(false);
    setSelected(val);
  };

  const removeItem = (id: string) => {
    console.log("removeItem", id);
    console.log("selected", selected);
    setSelected((prev) => prev.filter((item) => item !== id));
  }

  return {
    selected,
    removeItem,
    setPopupVisible: setVisible,
    visible,
    searchText,
    setSearchText,
    oneTerms,
    twoTerms,
    threeTerms,
    handleTermListChange,
  }
};

export default useTermSelectMultipleColumnsPopup;

type TermSelectionMultipleColumnsPopupProps = {
  visible: boolean;
  setVisible: (visible: boolean) => void;
  searchText: string;
  setSearchText: (searchText: string) => void;
  isMultiple: boolean;
  selected: string[];
  oneTerms: Term[];
  twoTerms: Term[];
  threeTerms: Term[];
  onChange: (val: Array<string>) => void;
};

export const TermSelectionMultipleColumnsPopup = ({ visible, setVisible, searchText, setSearchText, isMultiple, selected, oneTerms, twoTerms, threeTerms, onChange: handleTermListChange }: TermSelectionMultipleColumnsPopupProps) => {
  return (
    <Popup
      className="form-item-content--size__popup"
      visible={visible}
      onMaskClick={() => {
        setVisible(false);
      }}
      destroyOnClose
      position="top"
    >
      <div className="search-bar-container p-[12px] mt-7">
        <SearchBar
          placeholder="검색"
          value={searchText}
          onChange={(v) => {
            setSearchText(v);
          }}
        />
      </div>
      <div className="check-list-container">
        <CheckList
          multiple={isMultiple}
          defaultValue={[...selected]}
          onChange={handleTermListChange}
          extra={(active) => (active ? <CheckCircleFill /> : "")}
        >
          <div className="flex">
            <div className="flex-1">
              {oneTerms.map((item) => (
                <CheckList.Item key={item.term_id} value={`${item.term_id}`}>
                  {item.name.split("_")[0]}
                </CheckList.Item>
              ))}
            </div>
            <div className="flex-1">
              {twoTerms.map((item) => (
                <CheckList.Item key={item.term_id} value={`${item.term_id}`}>
                  {item.name.split("_")[0]}
                </CheckList.Item>
              ))}
            </div>
            <div className="flex-1">
              {threeTerms.map((item) => (
                <CheckList.Item key={item.term_id} value={`${item.term_id}`}>
                  {item.name.split("_")[0]}
                </CheckList.Item>
              ))}
            </div>
          </div>
        </CheckList>
      </div>
    </Popup>
  );
};
