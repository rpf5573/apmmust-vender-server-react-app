export type Term = {
  term_id: number;
  name: string;
  slug: string;
  description: string;
  count: number;
  parent: number;
};

export type Composition = {
  composition_fabric: Term;
  composition_fabric_rate: number; 
}

export type ProductCategoryTerm = Term & {
  root_slug: string;
  children: ProductCategoryTerm[];
};

export type MixingRateObj = Record<string, number>;

export type Product = {
  product_id: string;
  sku: string;
  product_name: string; // 서버에서 오지만 실제로 vendor plugin에서 사용하지는 않는다
  product_korean_name: string;
  product_english_name: string;
  regular_price: number;
  sale_price: number;
  weight: number;
  stock_status: string;
  created_at: string;
  modified_at: string;
  product_category_term: ProductCategoryTerm;
  size_term_list: Term[];
  colour_term_list: Term[];
  style_term_list: Term[];
  composition_list: Composition[];
  season_term: Term;
  made_in_term: Term;
  gender_term: Term;
  featured_image_url: string;
  gallery_image_url_list: string[];
};

export type API<Params, ResponseData> = {
  params: Params;
  response: {
    code: string;
    message: string;
    data: ResponseData;
  };
  variables: Params;
};

export type OrderItem = {
  product_id: number;
  sku: string;
  product_name: string;
  product_korean_name: string;
  subtotal: number;
  product_category_term: Term;
  colour_term_list: Term[];
  size_term_list: Term[];
  quantity: number;
};

export type Order = {
  order_id: number;
  order_date: string;
  customer_name: string;
  order_number: string;
  country_code_billing: string;
  item_list: OrderItem[];
  total: number;
};

export type User = {
  user_id: number;
  email: string;
  brand_name: string;
  brand_id: number;
  building: string;
  floor: string;
  room_number: string;
  bank: string;
  deposit_account: string;
  account_owner: string;
  phone_number: string;
  kakao_id: string;
  license_file_url: string;
  season_term?: Term;
  style_term_list?: Term[];
  made_in_term?: Term;
};

export type Notice = {
  id: number;
  title: string;
  content: string;
  author: string;
  created_at: string;
  updated_at: string;
};

export type Dashboard = {
  total_income: number;
  total_order_count: number;
  total_product_count: number;
  total_product_instock: number;
  total_product_outofstock: number;
  total_income_this_month: number;
};

export type ProductCategoryRootSlug =
  | "men"
  | "women"
  | "unisex"
  | "accessories";
