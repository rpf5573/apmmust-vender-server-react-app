import { Pagination } from "antd";
import { List, NavBar, NoticeBar } from "antd-mobile";
import type { FC } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { useGetOrderList } from "./order-list-api";
import OrderListItem from "./OrderListItem";
import GlobalLoading from "@/components/@shared/global-loading/GlobalLoading";
import ErrorPage from "../error/ErrorPage";
import KakaoFloatChatButton from "@/components/@shared/kakao-float-chat-button/KakaoFloatChatButton";
import useCheckKakaoIdAndLicenseStatus from "@/common/hooks/useCheckKakaoIdAndLicenseStatus";

type OrderListPageProps = any;
const OrderListPage: FC<OrderListPageProps> = () => {
  const navigate = useNavigate();

  const params = useParams();
  const page = Number(params["page"] ?? 1);

  const geOrderListQuery = useGetOrderList({ orderListCountPerPage: 10, page });
  const getMyAccountQuery = useCheckKakaoIdAndLicenseStatus();

  if (getMyAccountQuery.isLoading) {
    return <GlobalLoading visible={true} />;
  }

  if (getMyAccountQuery.isError) {
    // one of exist error
    const error = [
      getMyAccountQuery,
    ].find((query) => query.isError)?.error;
    return <ErrorPage error={error} />;
  }

  if (geOrderListQuery.isLoading) {
    return <GlobalLoading visible={true} />;
  }

  if (geOrderListQuery.isError) {
    // one of exist error
    const error = [
      geOrderListQuery,
    ].find((query) => query.isError)?.error;
    return <ErrorPage error={error} />;
  }

  const orderList = geOrderListQuery.data?.data.order_list ?? [];
  const totalCount = geOrderListQuery.data?.data.total_count ?? 0;

  console.log("orderList", orderList);

  const handleClickPaginationItem = (page: number) => {
    navigate(`/order-list/${page}`);
  };

  return (
    <div className="order-list-page has-pagination">
      <div className="top">
        <NavBar backArrow={false}>주문 목록</NavBar>
        <NoticeBar
          content="최신 데이터를 보려면 새로고침 해주세요"
          color="alert"
        />
        <List className="pb-3 flex-1">
          {orderList.map((order) => (
            <OrderListItem key={order.order_id} order={order} />
          ))}
        </List>
      </div>
      <div className="bottom">
        <div className="flex justify-center">
          <Pagination
            current={page}
            defaultCurrent={1}
            total={totalCount}
            pageSize={10}
            onChange={handleClickPaginationItem}
            showSizeChanger={false}
            disabled={geOrderListQuery.isLoading}
          />
        </div>
      </div>
      <KakaoFloatChatButton />
    </div>
  );
};

export default OrderListPage;
