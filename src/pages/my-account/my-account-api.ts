import axiosInstance from "@/axiosInstance";
import { API, User } from "@/common/types";
import { useQuery } from "@tanstack/react-query";
import { AxiosResponse } from "axios";

export type ApiGetMyAccount = API<unknown, User>;

// delete product
export const getMyAccount = async () => {
  const url = `${import.meta.env.VITE_API_URL}/my-account/get-my-account`;
  const response = await axiosInstance.get<
    ApiGetMyAccount["response"],
    AxiosResponse<ApiGetMyAccount["response"]>
  >(url, {
    timeout: 3000,
  });

  return response.data;
};

export const useGetMyAccount = () =>
  useQuery<ApiGetMyAccount["response"]>({
    queryKey: ["get-my-account"],
    queryFn: getMyAccount,
    staleTime: 1000 * 60 * 60 * 24, // 1 day
    gcTime: 1000 * 60 * 60 * 24, // 1 day
  });

export default useGetMyAccount;
