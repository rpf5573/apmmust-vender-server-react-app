import axiosInstance from "@/axiosInstance";
import { API, Order } from "@/common/types";
import { useQuery } from "@tanstack/react-query";
import { AxiosResponse } from "axios";

export type ApiGetOrderDetail = API<unknown, Order>;

// delete product
export const getOrderDetail = async (order_id: string) => {
  const url = `${
    import.meta.env.VITE_API_URL
  }/order/get-order-detail?order_id=${order_id}`;
  const response = await axiosInstance.get<
    ApiGetOrderDetail["response"],
    AxiosResponse<ApiGetOrderDetail["response"]>
  >(url, {
    timeout: 4000,
  });

  return response.data;
};

export const useGetOrderDetail = (order_id: string) =>
  useQuery<ApiGetOrderDetail["response"]>({
    queryKey: ["get-order-detail", order_id],
    queryFn: () => getOrderDetail(order_id),
    staleTime: 1000 * 60 * 60 * 24, // 24시간 동안 캐싱. 주문 정보는 잘 변하지 않기 때문에 24시간으로 캐싱한다
    gcTime: 1000 * 60 * 60 * 24, // 24시간 동안 캐싱. 주문 정보는 잘 변하지 않기 때문에 24시간으로 캐싱한다
  });

export default useGetOrderDetail;
