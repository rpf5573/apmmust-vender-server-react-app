import React, { useState, type FC, useEffect } from "react";
import { Controller, useForm, useFormContext } from "react-hook-form";
import { useNavigate, useParams } from "react-router-dom";
import useGetProductDetail from "../product-detail/product-detail-api";
import useGetSizeTermList from "@/common/api/api-get-size-term-list";
import useGetProductCategoryTermTree from "@/common/api/api-get-product-category-term-tree";
import useGetColourTermList from "@/common/api/api-get-colour-term-list";
import useGetSeasonTermList from "@/common/api/api-get-season-term-list";
import useGetStyleTermList from "@/common/api/api-get-style-term-list";
import useGetMadeInTermList from "@/common/api/api-get-made-in-term-list";
import CustomFormItem from "@/components/@shared/custom-form-item/CustomFormItem";
import { Button, CascaderView, Collapse, Input, List, NavBar } from "antd-mobile";
import { Product, ProductCategoryTerm, Term } from "@/common/types";
import {
  InputNumber,
  Upload,
  message,
  Button as AntdButton,
  UploadFile,
} from "antd";
import { uploadImage } from "@/common/api/api-s3";
import {
  findProductCategoryTermAncestors,
  findTermById,
  transformTermListForCascaderViewOption,
} from "@/common/utils";
import { FIELD_NAME } from "@/constants";
import { FormProvider } from "react-hook-form";
import TermSelectFormItem from "@/components/@shared/term-select-form-item/TermSelectFormItem";
import _ from "underscore";
import "./edit-product.scss";
import { editProduct } from "./edit-product-api";
import GlobalLoading from "@/components/@shared/global-loading/GlobalLoading";
import { useQueryClient } from "@tanstack/react-query";
import { UploadOutline } from "antd-mobile-icons";
import useScrollToTopAfterMount from "@/common/hooks/useScrollToTopAfterMount";
import ErrorPage from "../error/ErrorPage";
import TermSelectFormItemMultipleColumn from "@/components/@shared/term-select-form-item/TermSelectFormItemMultipleColumn";
import { commaizeNumber, decommaizeNumber } from "@toss/utils";
import useCheckKakaoIdAndLicenseStatus from "@/common/hooks/useCheckKakaoIdAndLicenseStatus";
import useTermSelectMultipleColumnsPopup, { TermSelectionMultipleColumnsPopup } from "@/common/hooks/useTermSelectMultipleCulumnsPopup";
import useGetCompositionTermList from "@/common/api/api-get-composition-term-list";

type EditProductPageProps = any;
const EditProductPage: FC<EditProductPageProps> = () => {
  const form = useForm();
  const queryClient = useQueryClient();
  const errorCount = Object.keys(form.formState.errors).length;
  const navigate = useNavigate();

  useScrollToTopAfterMount();

  const params = useParams();
  const productId = params["product_id"]!;
  const getProductDetailQuery = useGetProductDetail(productId);
  const getSizeTermListQuery = useGetSizeTermList();
  const getColourTermListQuery = useGetColourTermList();
  const getProductCategoryTermTreeQuery = useGetProductCategoryTermTree();
  const getSeasonTermListQuery = useGetSeasonTermList();
  const getStyleTermListQuery = useGetStyleTermList();
  const getMadeInTermListQuery = useGetMadeInTermList();
  const getCompositionTermListQuery = useGetCompositionTermList();

  const [isFeaturedImageUploading, setIsFeaturedImageUploading] =
    useState(false);
  const [isGalleryImageUploading, setIsGalleryImageUploading] = useState(false);
  const [isProductEditing, setIsProductEditing] = useState(false);

  const getMyAccountQuery = useCheckKakaoIdAndLicenseStatus();

  if (getMyAccountQuery.isLoading) {
    return <GlobalLoading visible={true} />;
  }

  if (getMyAccountQuery.isError) {
    // one of exist error
    const error = [getMyAccountQuery].find((query) => query.isError)?.error;
    return <ErrorPage error={error} />;
  }

  if (
    getProductDetailQuery.isLoading ||
    getSizeTermListQuery.isLoading ||
    getColourTermListQuery.isLoading ||
    getProductCategoryTermTreeQuery.isLoading ||
    getSeasonTermListQuery.isLoading ||
    getStyleTermListQuery.isLoading ||
    getMadeInTermListQuery.isLoading || 
    getCompositionTermListQuery.isLoading
  ) {
    return <GlobalLoading visible={true} />;
  }

  if (
    getProductDetailQuery.isError ||
    getSizeTermListQuery.isError ||
    getColourTermListQuery.isError ||
    getProductCategoryTermTreeQuery.isError ||
    getSeasonTermListQuery.isError ||
    getStyleTermListQuery.isError ||
    getMadeInTermListQuery.isError ||
    getCompositionTermListQuery.isError
  ) {
    // one of exist error
    const error = [
      getProductDetailQuery,
      getSizeTermListQuery,
      getColourTermListQuery,
      getProductCategoryTermTreeQuery,
      getSeasonTermListQuery,
      getStyleTermListQuery,
      getMadeInTermListQuery,
    ].find((query) => query.isError)?.error;
    return <ErrorPage error={error} />;
  }

  const productDetail = getProductDetailQuery.data?.data;
  const sizeTermList = getSizeTermListQuery.data?.data;
  const colourTermList = getColourTermListQuery.data?.data;
  const productCategoryTermTree = getProductCategoryTermTreeQuery.data?.data;
  const seasonTermList = getSeasonTermListQuery.data?.data;
  const styleTermList = getStyleTermListQuery.data?.data;
  const madeInTermList = getMadeInTermListQuery.data?.data;
  const compositionTermList = getCompositionTermListQuery.data?.data;

  if (
    !productDetail ||
    !sizeTermList ||
    !colourTermList ||
    !productCategoryTermTree ||
    !seasonTermList ||
    !styleTermList ||
    !madeInTermList ||
    !compositionTermList
  ) {
    return <div>필요한 값이 없습니다</div>;
  }

  const onSubmit = (data: any) => {
    if (isProductEditing) return;

    setIsProductEditing(true);
    const product_id = productDetail.product_id;

    console.log('edit product data', data);

    // Extracting other properties
    const product_korean_name = data[FIELD_NAME.PRODUCT_KOREAN_NAME];
    const product_english_name = data[FIELD_NAME.PRODUCT_ENGLISH_NAME] ?? null;
    const regular_price = data[FIELD_NAME.REGULAR_PRICE]
      ? decommaizeNumber(data[FIELD_NAME.REGULAR_PRICE])
      : 0;
    const sale_price = data[FIELD_NAME.SALE_PRICE]
      ? decommaizeNumber(data[FIELD_NAME.SALE_PRICE])
      : 0;
    const weight = data[FIELD_NAME.WEIGHT] ?? null;
    const product_category_term_id = data[FIELD_NAME.PRODUCT_CATEGORY_TERM_ID];
    const style_term_id_list = data[FIELD_NAME.STYLE_TERM_ID_LIST] ?? null;
    const season_term_id = data[FIELD_NAME.SEASON_TERM_ID] ?? null;
    const featured_image_url =
      _.isArray(data[FIELD_NAME.FEATURED_IMAGE_URL]) &&
      data[FIELD_NAME.FEATURED_IMAGE_URL].length > 0
        ? data[FIELD_NAME.FEATURED_IMAGE_URL][0]
        : null;
    const gallery_image_url_list =
      data[FIELD_NAME.GALLERY_IMAGE_URL_LIST] ?? null;
    const made_in_term_id = data[FIELD_NAME.MADE_IN_TERM_ID] ?? null;
    // const size_term_id_list = data[FIELD_NAME.SIZE_TERM_ID_LIST] ?? null;
    // const colour_term_id_list = data[FIELD_NAME.COLOUR_TERM_ID_LIST] ?? null;
    let composition_list = data[FIELD_NAME.COMPOSITION_LIST] ?? null;

    // composition list를 metabox format에 맞게 변경해준다
    if (composition_list) {
      composition_list = Object.keys(composition_list).map((slug) => {
        return {
          composition_fabric: slug,
          composition_fabric_rate: Number(composition_list[slug]),
        };
      });
    }

    const product = {
      product_id,
      product_korean_name,
      product_english_name,
      regular_price,
      sale_price,
      weight,
      product_category_term_id,
      style_term_id_list,
      season_term_id,
      featured_image_url,
      gallery_image_url_list,
      made_in_term_id,
      composition_list,
    };

    editProduct(product)
      .then(() => {
        // product 캐쉬를 삭제한다
        queryClient.removeQueries({
          queryKey: ["get-product-detail", `${product_id}`],
        });

        // product list 캐쉬를 삭제한다
        queryClient.removeQueries({
          queryKey: ["get-product-list"],
        });

        message.success("상품이 수정되었습니다");
        // navigate("/product-detail/" + product_id);
      })
      .catch((err) => {
        const errorMessage = err.response.data.message;
        message.error(`생성 실패 - ${errorMessage}`);
      })
      .finally(() => {
        setIsProductEditing(false);
      });
  };

  return (
    <div className="edit-product-page">
      <FormProvider {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)} className="adm-form">
          <NavBar onBack={() => navigate(-1)}>상품 수정</NavBar>
          <List>
            {/* 한글 상품명 */}
            <KoreanNameFormItem product={productDetail} />

            {/* 영문 상품명 */}
            <EnglishNameFormItem product={productDetail} />

            {/* 상품 가격 */}
            <RegularPriceFormItem product={productDetail} />

            {/* 할인 가격 */}
            <SalePriceFormItem product={productDetail} />

            {/* 상품 카테고리 */}
            <ProductCategoryFormItem
              product={productDetail}
              termList={productCategoryTermTree}
            />

            {/* 특성 이미지 */}
            <FeaturedImageFormItem
              product={productDetail}
              isFeaturedImageUploading={isFeaturedImageUploading}
              setIsFeaturedImageUploading={setIsFeaturedImageUploading}
            />

            {/* 갤러리 이미지 */}
            <GalleryImageFormItem
              product={productDetail}
              isGalleryImageUploading={isGalleryImageUploading}
              setIsGalleryImageUploading={setIsGalleryImageUploading}
            />

            {/* 무게 */}
            <WeightFormItem product={productDetail} />

            {/* 사이즈 */}
            <SizeFormItem product={productDetail} termList={sizeTermList} />

            {/* 색상 */}
            <ColourFormItem product={productDetail} termList={colourTermList} />

            {/* 시즌 */}
            <SeasonFormItem product={productDetail} termList={seasonTermList} />

            {/* 스타일 */}
            <StyleFormItem product={productDetail} termList={styleTermList} />

            {/* 제조국 */}
            <MadeInFormItem product={productDetail} termList={madeInTermList} />

            {/* 혼용률 */}
            <CompositionFormItem product={productDetail} termList={compositionTermList} />
          </List>
          <div className="adm-form-footer">
            <Button
              className="!mb-2"
              block
              type="submit"
              color="primary"
              size="large"
              disabled={
                isFeaturedImageUploading &&
                isGalleryImageUploading &&
                Object.keys(form.formState.errors).length > 0
              }
              loading={isProductEditing}
            >
              수정하기
            </Button>
            {errorCount > 0 && (
              <div className="text-center">
                <span className="text-red-500">필수값을 확인해주세요</span>
              </div>
            )}
          </div>
        </form>
      </FormProvider>
    </div>
  );
};

export default EditProductPage;

const KoreanNameFormItem = ({ product }: { product: Product }) => {
  const form = useFormContext();

  return (
    <Controller
      name={FIELD_NAME.PRODUCT_KOREAN_NAME}
      defaultValue={product.product_korean_name ?? ""}
      control={form.control}
      render={({
        field: { ref, ...field },
        fieldState: { invalid, error },
      }) => {
        return (
          <CustomFormItem
            label="한글 상품명"
            required
            showError={invalid}
            errorMessage={error?.message}
          >
            <Input placeholder="상품 이름 입력" {...field} ref={ref} />
          </CustomFormItem>
        );
      }}
      rules={{ required: "상품명을 입력해주세요" }}
    />
  );
};

const EnglishNameFormItem = ({ product }: { product: Product }) => {
  const form = useFormContext();

  return (
    <Controller
      name={FIELD_NAME.PRODUCT_ENGLISH_NAME}
      defaultValue={product.product_name ?? ""}
      control={form.control}
      render={({
        field: { ref, ...field },
        fieldState: { invalid, error },
      }) => {
        return (
          <CustomFormItem
            label="영문 상품명"
            showError={invalid}
            errorMessage={error?.message}
          >
            <Input
              placeholder="영문 상품명 입력시 고객에게 노출 될 확률 올라갑니다"
              {...field}
              ref={ref}
            />
          </CustomFormItem>
        );
      }}
    />
  );
};

const RegularPriceFormItem = ({ product }: { product: Product }) => {
  const form = useFormContext();

  return (
    <Controller
      name={FIELD_NAME.REGULAR_PRICE}
      defaultValue={commaizeNumber(product.regular_price)}
      control={form.control}
      render={({ field, fieldState: { invalid, error } }) => (
        <CustomFormItem
          label="가격"
          required
          showError={invalid}
          errorMessage={error?.message}
        >
          <Input
            placeholder="상품 가격 입력"
            {...field}
            onChange={(e) => {
              const num = decommaizeNumber(e);
              if (isNaN(Number(num))) {
                field.onChange("");
                return;
              }
              const formattedValue = commaizeNumber(num);
              field.onChange(formattedValue === "0" ? "" : formattedValue);
            }}
          />
        </CustomFormItem>
      )}
      rules={{
        required: "가격을 입력해주세요",
      }}
    />
  );
};

const SalePriceFormItem = ({ product }: { product: Product }) => {
  const form = useFormContext();

  return (
    <Controller
      name={FIELD_NAME.SALE_PRICE}
      defaultValue={commaizeNumber(product.sale_price)}
      control={form.control}
      render={({ field, fieldState: { invalid, error } }) => (
        <CustomFormItem
          label="할인 가격"
          showError={invalid}
          errorMessage={error?.message}
        >
          <Input
            placeholder="할인된 상품 가격 입력"
            {...field}
            onChange={(e) => {
              // check e is number or not, e is string now
              const num = decommaizeNumber(e);
              if (isNaN(Number(num))) {
                field.onChange("");
                return;
              }
              const formattedValue = commaizeNumber(num);
              field.onChange(formattedValue === "0" ? "" : formattedValue);
            }}
          />
        </CustomFormItem>
      )}
      rules={{
        min: { value: 0, message: "할인 가격은 0원 이상이어야 합니다" },
      }}
    />
  );
};

const FeaturedImageFormItem = ({
  product,
  isFeaturedImageUploading,
  setIsFeaturedImageUploading,
}: {
  product: Product;
  isFeaturedImageUploading: boolean;
  setIsFeaturedImageUploading: React.Dispatch<React.SetStateAction<boolean>>;
}) => {
  const form = useFormContext();
  const [selected, setSelected] = useState<Array<UploadFile>>([
    {
      uid: product.sku,
      name:
        product.featured_image_url.split("/").pop() ??
        product.featured_image_url,
      url: product.featured_image_url,
    },
  ]);

  useEffect(() => {
    form.setValue(
      FIELD_NAME.FEATURED_IMAGE_URL,
      selected.map((item) => item.url)
    );
    form.clearErrors(FIELD_NAME.FEATURED_IMAGE_URL);
  }, [selected]);

  async function handleUpload(file: any) {
    try {
      const imageUrl = await uploadImage(file);
      return imageUrl;
    } catch (err) {
      message.error("이미지 업로드에 실패했습니다");
      throw err;
    }
  }

  return (
    <Controller
      name={FIELD_NAME.FEATURED_IMAGE_URL}
      control={form.control}
      render={({ fieldState: { invalid, error } }) => (
        <CustomFormItem
          label="대표 이미지"
          required
          showError={invalid}
          errorMessage={error?.message}
        >
          <Upload
            beforeUpload={(file) => {
              return new Promise((resolve, reject) => {
                // file size limit is 5mb
                if (file.size > 5 * 1024 * 1024) {
                  message.error("이미지 파일은 5MB 이하만 업로드 가능합니다");
                  return reject();
                }
                resolve();
              });
            }}
            customRequest={({ file }) => {
              setIsFeaturedImageUploading(true);
              handleUpload(file)
                .then(({ imageUrl, imageKey, fileName }) => {
                  const uploadFile: UploadFile = {
                    uid: imageKey,
                    name: fileName,
                    url: imageUrl,
                    status: "done",
                  };
                  setSelected((prev) => [...prev, uploadFile]);
                })
                .catch(() => {
                  alert(
                    `이미지 업로드중 오류가 발생했습니다 - 파일명: ${
                      (file as any).name
                    }`
                  );
                  window.location.reload();
                })
                .finally(() => {
                  setIsFeaturedImageUploading(false);
                });
            }}
            fileList={selected}
            listType="picture"
            maxCount={1}
            multiple={false}
            accept="image/*"
            onChange={({ fileList }) => {
              setSelected(fileList.filter((file) => file.status === "done"));
            }}
            disabled={isFeaturedImageUploading}
          >
            <AntdButton
              icon={<UploadOutline />}
              loading={isFeaturedImageUploading}
              disabled={isFeaturedImageUploading}
            >
              업로드
            </AntdButton>
          </Upload>
        </CustomFormItem>
      )}
      rules={{ required: "대표 이미지를 선택해주세요" }}
    />
  );
};

const GalleryImageFormItem = ({
  product,
  isGalleryImageUploading,
  setIsGalleryImageUploading,
}: {
  product: Product;
  isGalleryImageUploading: boolean;
  setIsGalleryImageUploading: React.Dispatch<React.SetStateAction<boolean>>;
}) => {
  const maxCount = 30;
  const form = useFormContext();
  const uploadedFiles = (product.gallery_image_url_list ?? []).map((url) => ({
    uid: url,
    name: url.split("/").pop() ?? url,
    url: url,
    status: "done",
  })) as Array<UploadFile>;

  console.log("uploadedFiles", uploadedFiles);

  const [selected, setSelected] = useState<Array<UploadFile>>(() => [
    ...uploadedFiles,
  ]);
  const [임시파일목록, set임시파일목록] = useState<Array<UploadFile>>(() => [
    ...uploadedFiles,
  ]);
  const [sortedSelected, setSortedSelected] = useState<Array<UploadFile>>([]); // 업로드된 순서대로 정렬된 selected

  // @TODO: dependency에 form을 넣으면 리렌더링이 계속 발생한다
  useEffect(() => {
    const newSortedSelected = 임시파일목록.reduce((prev, curr) => {
      const found = selected.find((item) => item.name === curr.name);
      if (found && prev.indexOf(found) === -1) {
        prev.push(found);
      }
      return prev;
    }, [] as Array<UploadFile>);
    setSortedSelected(newSortedSelected);
  }, [selected, 임시파일목록]);

  useEffect(() => {
    form.setValue(
      FIELD_NAME.GALLERY_IMAGE_URL_LIST,
      sortedSelected.map((item) => item.url)
    );
    form.clearErrors(FIELD_NAME.GALLERY_IMAGE_URL_LIST);
  }, [sortedSelected]);

  async function handleUpload(file: any) {
    try {
      const imageUrl = await uploadImage(file);
      return imageUrl;
    } catch (err) {
      message.error("이미지 업로드에 실패했습니다");
      throw err;
    }
  }

  return (
    <Controller
      name={FIELD_NAME.GALLERY_IMAGE_URL_LIST}
      control={form.control}
      render={({ fieldState: { invalid, error } }) => (
        <CustomFormItem
          label="갤러리 이미지"
          showError={invalid}
          errorMessage={error?.message}
          className="gallery-image-form-item"
        >
          <Upload
            beforeUpload={(file, fileList) => {
              return new Promise((resolve, reject) => {
                if (fileList.length + sortedSelected.length > maxCount) {
                  message.destroy("gallery-image-upload"); // 1개만 보여져야 하기 때문에 이전것은 삭제한다
                  message.error({
                    content: `갤러리 이미지는 ${maxCount}개까지만 업로드 가능합니다`,
                    duration: 5,
                    key: "gallery-image-upload",
                  });
                  return reject();
                }
                // file size limit is 5mb
                if (file.size > 5 * 1024 * 1024) {
                  message.error("이미지 파일은 5MB 이하만 업로드 가능합니다");
                  return reject();
                }
                resolve();
              });
            }}
            customRequest={({ file }) => {
              setIsGalleryImageUploading(true);
              handleUpload(file)
                .then(({ imageUrl, imageKey, fileName }) => {
                  const uploadFile: UploadFile = {
                    uid: imageKey,
                    name: fileName,
                    url: imageUrl,
                    status: "done",
                  };
                  setSelected((prev) => [...prev, uploadFile]);
                })
                .catch(() => {
                  alert(
                    `이미지 업로드중 오류가 발생했습니다 - 파일명: ${
                      (file as any).name
                    }`
                  );
                  window.location.reload();
                })
                .finally(() => {
                  setIsGalleryImageUploading(false);
                });
            }}
            fileList={sortedSelected}
            listType="picture"
            multiple
            maxCount={maxCount}
            accept="image/*"
            onChange={({ fileList }) => {
              set임시파일목록([...fileList]);
            }}
            disabled={isGalleryImageUploading}
          >
            <AntdButton
              icon={<UploadOutline />}
              loading={isGalleryImageUploading}
              disabled={isGalleryImageUploading}
            >
              업로드 (최대 {maxCount}개)
            </AntdButton>
          </Upload>
        </CustomFormItem>
      )}
    />
  );
};

const WeightFormItem = ({ product }: { product: Product }) => {
  const form = useFormContext();

  return (
    <Controller
      name={FIELD_NAME.WEIGHT}
      control={form.control}
      defaultValue={product.weight ?? 0}
      render={({ field, fieldState: { invalid, error } }) => (
        <CustomFormItem
          label="무게(g)"
          showError={invalid}
          errorMessage={error?.message}
        >
          <Input type="number" placeholder="무게(g) 입력" {...field} />
        </CustomFormItem>
      )}
      rules={{ min: { value: 0, message: "무게는 0g 이상이어야 합니다" } }}
    />
  );
};

const SizeFormItem = ({
  product,
  termList,
}: {
  product: Product;
  termList: Term[];
}) => {
  const form = useFormContext();

  return (
    <TermSelectFormItemMultipleColumn
      defaultTermList={product.size_term_list}
      termList={termList}
      isMultiple={true}
      fieldName={FIELD_NAME.SIZE_TERM_ID_LIST}
      label="사이즈"
      form={form}
      required={true}
      errorMessage="사이즈를 선택해주세요"
      disabled
    />
  );
};

const ColourFormItem = ({
  product,
  termList,
}: {
  product: Product;
  termList: Term[];
}) => {
  const form = useFormContext();

  return (
    <TermSelectFormItemMultipleColumn
      defaultTermList={product.colour_term_list}
      termList={termList}
      isMultiple={true}
      fieldName={FIELD_NAME.COLOUR_TERM_ID_LIST}
      label="색상"
      form={form}
      required={true}
      errorMessage="색상을 선택해주세요"
      disabled
    />
  );
};

const SeasonFormItem = ({
  product,
  termList,
}: {
  product: Product;
  termList: Term[];
}) => {
  const form = useFormContext();

  return (
    <TermSelectFormItem
      defaultTermList={[product.season_term]}
      termList={termList}
      isMultiple={false}
      fieldName={FIELD_NAME.SEASON_TERM_ID}
      label="시즌"
      form={form}
      required={true}
      errorMessage="시즌을 선택해주세요"
    />
  );
};

const StyleFormItem = ({
  product,
  termList,
}: {
  product: Product;
  termList: Term[];
}) => {
  const form = useFormContext();

  return (
    <TermSelectFormItem
      defaultTermList={product.style_term_list}
      termList={termList}
      isMultiple={true}
      fieldName={FIELD_NAME.STYLE_TERM_ID_LIST}
      label="스타일"
      form={form}
      required={true}
      errorMessage="스타일을 선택해주세요"
    />
  );
};

const MadeInFormItem = ({
  product,
  termList,
}: {
  product: Product;
  termList: Term[];
}) => {
  const form = useFormContext();

  return (
    <TermSelectFormItem
      defaultTermList={[product.made_in_term]}
      termList={termList}
      isMultiple={false}
      fieldName={FIELD_NAME.MADE_IN_TERM_ID}
      label="제조국"
      form={form}
      required={true}
      errorMessage="제조국을 선택해주세요"
    />
  );
};

// 주의: selected에 들어있는건 slug가 아니라 term_id이다 / form.unregister를 해줘야 한다.
// 이게 한번 register하면 ListItem컴포넌트가 렌더링 안되도 react-hook-form은 내부적으로 계속 관리하고 있다.
const CompositionFormItem = ({ product, termList }: { product: Product; termList: Term[] }) => {
  const form = useFormContext();

  const {
    selected,
    setPopupVisible,
    visible,
    searchText,
    setSearchText,
    oneTerms,
    twoTerms,
    threeTerms,
    handleTermListChange: handlefabricListChange,
    removeItem,
  } = useTermSelectMultipleColumnsPopup({
    defaultTermList: product.composition_list.map((composition) => {
      return composition.composition_fabric;
    }), // 이곳에 product.composition_list를 넣어주면 된다
    termList,
    isMultiple: false,
  });

  const handleRemoveItemBtnClick = (fieldName: string) => {
    // find termId with fieldName from termList
    const term = termList.find((term) => `${FIELD_NAME.COMPOSITION_LIST}.${term.slug}` === fieldName);
    term && removeItem(`${term.term_id}`);
    // de-register from form
    form.unregister(fieldName);
  }

  // 이야 아이디어 기가 맥힌다잉?
  // 렌더링 시점에 찾아서 넣으면 랜더링 할때마다 새로운 값을 못넣고 계속 이전값이 보일텐데
  // 이렇게 마운트 되고 나서 딱 하는거면 딱이지!
  // 그런데 이 컴포넌트가 다시 마운트 된다면 어떻게 될까...? 그럴일은 없다고 상정하는게 맞긴 하겠지?
  useEffect(() => {
    product.composition_list.forEach((composition) => {
      const fieldName = `${FIELD_NAME.COMPOSITION_LIST}.${composition.composition_fabric.slug}`;
      form.register(fieldName);
      form.setValue(fieldName, composition.composition_fabric_rate);
    });
  }, []);

  const ListItem = ({
    fieldName,
    label,
  }: {
    fieldName: string;
    label: string;
  }) => (
    <Controller
      name={fieldName}
      control={form.control}
      render={({ field, fieldState: { error } }) => (
        <List.Item
          key={fieldName}
          extra={
            <div className="flex gap-2 items-center">
              <InputNumber min={0} max={100} step={1} addonAfter="%" {...field}/>
              <button type="button" className="break-words" onClick={() => handleRemoveItemBtnClick(fieldName)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="red" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-x">
                  <line x1="18" y1="6" x2="6" y2="18"></line>
                  <line x1="6" y1="6" x2="18" y2="18"></line>
                </svg>
              </button>
            </div>
          }
        >
          <span>{label}</span>
          <div className="adm-list-item-description">
            {error && (
              <div className="adm-form-item-feedback-error">
                {error.message ?? "다시 확인해 주세요"}
              </div>
            )}
          </div>
        </List.Item>
      )}
    />
  );

  return (
    <Collapse accordion defaultActiveKey="1">
      <Collapse.Panel key="1" title="소재 - 혼합률">
        <List>
          {selected.map((term_id) => {
            const term = findTermById(termList, term_id);
            if (!term) return null;
            return <ListItem key={term?.term_id} fieldName={`${FIELD_NAME.COMPOSITION_LIST}.${term?.slug}`} label={term?.name} />
          })}
          <div className="my-2">
            <Button
              onClick={() => {
                setPopupVisible(true);
              }}
              block
            >
              소재 추가하기
            </Button>
          </div>
        </List>
        <TermSelectionMultipleColumnsPopup
          visible={visible}
          setVisible={setPopupVisible}
          searchText={searchText}
          setSearchText={setSearchText}
          isMultiple={true}
          selected={selected}
          oneTerms={oneTerms}
          twoTerms={twoTerms}
          threeTerms={threeTerms}
          onChange={handlefabricListChange}
        />
      </Collapse.Panel>
    </Collapse>
  );
};

const ProductCategoryFormItem = ({
  product,
  termList,
}: {
  product: Product;
  termList: ProductCategoryTerm[];
}) => {
  const form = useFormContext();
  const productCategoryTermId = product.product_category_term.term_id;
  const [selected, setSelected] = useState<string[]>(() => {
    return productCategoryTermId
      ? findProductCategoryTermAncestors(productCategoryTermId, termList).map(
          String
        )
      : [];
  });

  console.log("selected", selected);

  // Update the form when selected changes
  useEffect(() => {
    // form으로 전달 할때는 숫자를 넣어야 하기 때문에, Number로 변환해준다
    form.setValue(
      FIELD_NAME.PRODUCT_CATEGORY_TERM_ID,
      selected.length > 0 ? Number(selected[selected.length - 1]) : null
    );
    form.clearErrors(FIELD_NAME.PRODUCT_CATEGORY_TERM_ID);
  }, [selected]);

  // Transform the term list for the Cascader view
  const transformedTermList = transformTermListForCascaderViewOption(termList);

  return (
    <Controller
      name={FIELD_NAME.PRODUCT_CATEGORY_TERM_ID}
      control={form.control}
      render={({ fieldState: { invalid, error } }) => (
        <CustomFormItem
          label="상품 카테고리"
          required
          showError={invalid}
          errorMessage={error?.message}
        >
          <CascaderView
            options={transformedTermList}
            value={selected}
            className="product-category-cascader-view"
            onChange={(val) => {
              setSelected(val);
            }}
          />
        </CustomFormItem>
      )}
      rules={{ required: "카테고리를 선택해주세요" }}
    />
  );
};
