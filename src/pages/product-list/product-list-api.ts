import axiosInstance from "@/axiosInstance";
import { API, Product } from "@/common/types";
import { useQuery } from "@tanstack/react-query";
import { AxiosResponse } from "axios";

export type ApiGetProductList = API<
  {
    productListCountPerPage: number;
    page: number;
    productKoreanName?: string;
    stockStatus?: string;
    productCategoryTermIdListStr?: string;
    startDate?: string;
    endDate?: string;
  },
  {
    product_list: Product[];
    total_count: number;
    current_page: number;
  }
>;

export const getProductList = async ({
  productListCountPerPage,
  page,
  productKoreanName,
  stockStatus,
  productCategoryTermIdListStr,
  startDate,
  endDate,
}: ApiGetProductList["params"]) => {
  let url = `${
    import.meta.env.VITE_API_URL
  }/product/get-product-list?page=${page}&product_list_count_per_page=${productListCountPerPage}`;

  if (productKoreanName) {
    url += `&product_korean_name=${productKoreanName}`;
  }

  if (stockStatus) {
    url += `&stock_status=${stockStatus}`;
  }

  if (productCategoryTermIdListStr) {
    url += `&product_category_term_id_list=${productCategoryTermIdListStr}`; // 서버에서는 알아서 배열로 해석한다
  }

  if (startDate) {
    url += `&start_date=${startDate}`;
  }

  if (endDate) {
    url += `&end_date=${endDate}`;
  }

  const response = await axiosInstance.get<
    ApiGetProductList["params"],
    AxiosResponse<ApiGetProductList["response"]>
  >(url, {
    timeout: 8000,
  });

  return response.data;
};

export const useGetProductList = ({
  productListCountPerPage,
  page,
  productKoreanName,
  stockStatus,
  productCategoryTermIdListStr,
  startDate,
  endDate,
}: ApiGetProductList["params"]) => {
  const key = [
    "get-product-list",
    page,
    productListCountPerPage,
    productKoreanName,
    stockStatus,
    productCategoryTermIdListStr,
    startDate,
    endDate,
  ].filter((item) => !!item);
  return useQuery<ApiGetProductList["response"]>({
    queryKey: key,
    queryFn: () =>
      getProductList({
        page,
        productListCountPerPage,
        productKoreanName,
        stockStatus,
        productCategoryTermIdListStr,
        startDate,
        endDate,
      }),
    staleTime: 1000 * 60 * 60 * 24, // 24시간 동안 캐싱
    gcTime: 1000 * 60 * 60 * 24, // 24시간 동안 캐싱
  });
};

export type ApiEditProductModifiedDateToNow = API<
  {
    product_id: number;
  },
  null
>;

export const editProductModifiedDateToNow = async (productId: string) => {
  const url = `${
    import.meta.env.VITE_API_URL
  }/product/edit-product-modified-date-to-now/?product_id=${productId}`;
  const response = await axiosInstance.patch<
    ApiEditProductModifiedDateToNow["params"],
    AxiosResponse<ApiEditProductModifiedDateToNow["response"]>
  >(url, {
    timeout: 10000,
  });
  return response.data;
};

export type ApiEditProductStockStatus = API<
  {
    product_id: string;
    stock_status: "instock" | "outofstock";
  },
  null
>;

export const editProductStockStatus = async ({
  product_id,
  stock_status,
}: ApiEditProductStockStatus["params"]) => {
  const url = `${
    import.meta.env.VITE_API_URL
  }/product/edit-product-stock-status`;
  const response = await axiosInstance.patch<
    ApiEditProductModifiedDateToNow["params"],
    AxiosResponse<ApiEditProductModifiedDateToNow["response"]>
  >(
    url,
    {
      product_id,
      stock_status,
    },
    {
      timeout: 10000,
    }
  );
  return response.data;
};
