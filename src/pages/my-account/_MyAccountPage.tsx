import { Button, List, NavBar } from "antd-mobile";
import type { FC } from "react";
import { useNavigate } from "react-router-dom";
import { useGetMyAccount } from "./my-account-api";
import GlobalLoading from "@/components/@shared/global-loading/GlobalLoading";
import TokenContoller from "@/TokenController";
import ErrorPage from "../error/ErrorPage";
import KakaoFloatChatButton from "@/components/@shared/kakao-float-chat-button/KakaoFloatChatButton";

type MyAccountPageProps = any;
const MyAccountPage: FC<MyAccountPageProps> = () => {
  const navigate = useNavigate();

  const getMyAccountQuery = useGetMyAccount();

  const handleEditButtonClick = () => {
    navigate("/edit-my-account");
  };

  const handleLogoutButtonClick = () => {
    TokenContoller.clear();
    window.location.href = "/";
  };

  if (getMyAccountQuery.isLoading) {
    return <GlobalLoading visible={true} />;
  }

  if (getMyAccountQuery.isError) {
    // one of exist error
    const error = [
      getMyAccountQuery,
    ].find((query) => query.isError)?.error;
    return <ErrorPage error={error} />;
  }

  const user = getMyAccountQuery.data?.data ?? null;

  if (!user) {
    return <div>사용자 정보가 존재하지 않습니다.</div>;
  }

  const {
    email,
    brand_name,
    building,
    floor,
    room_number,
    bank,
    deposit_account,
    account_owner,
    phone_number,
    season_term,
    made_in_term,
    style_term_list,
    kakao_id,
    license_file_url,
  } = user;

  return (
    <div className="my-account-page">
      <NavBar backArrow={false}>마이 페이지</NavBar>
      <List header="내 정보">
        <List.Item extra={email ?? "-"}>이메일</List.Item>
        <List.Item extra={brand_name ?? "-"}>브랜드</List.Item>
        <List.Item
          extra={style_term_list?.map((term) => term.name).join(", ") ?? "-"}
        >
          스타일
        </List.Item>
        <List.Item extra={season_term?.name ?? "-"}>시즌</List.Item>
        <List.Item extra={made_in_term?.name ?? "-"}>제조국</List.Item>
        <List.Item extra={building ? building : "-"}>건물</List.Item>
        <List.Item extra={floor ? floor : "-"}>층</List.Item>
        <List.Item extra={room_number ? room_number : "-"}>호수</List.Item>
        <List.Item extra={bank ? bank : "-"}>은행</List.Item>
        <List.Item extra={deposit_account ? deposit_account : "-"}>계좌번호</List.Item>
        <List.Item extra={account_owner ? account_owner : "-"}>예금주</List.Item>
        <List.Item extra={phone_number ? phone_number : "-"}>매장 폰 번호</List.Item>
        <List.Item extra={kakao_id ? kakao_id : "-"}>카카오톡 아이디</List.Item>
        <List.Item extra={license_file_url ? <a href={license_file_url} target="_blank" className="license-file">파일 다운로드</a> : "-"}>사업자 등록증</List.Item>
      </List>
      <div className="product-actions flex flex-col gap-3 px-3 py-3">
        <Button
          block
          color="primary"
          size="large"
          onClick={handleEditButtonClick}
        >
          수정하기
        </Button>
        <Button
          block
          color="warning"
          size="large"
          onClick={handleLogoutButtonClick}
        >
          로그아웃
        </Button>
      </div>
      <KakaoFloatChatButton />
    </div>
  );
};

export default MyAccountPage;
