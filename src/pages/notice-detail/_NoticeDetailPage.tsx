import { NavBar } from "antd-mobile";
import type { FC } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { useGetNoticeDetail } from "./notice-detail-api";
import GlobalLoading from "@/components/@shared/global-loading/GlobalLoading";
import useScrollToTopAfterMount from "@/common/hooks/useScrollToTopAfterMount";
import ErrorPage from "../error/ErrorPage";
import useCheckKakaoIdAndLicenseStatus from "@/common/hooks/useCheckKakaoIdAndLicenseStatus";

type NoticeDetailPageProps = any;
const NoticeDetailPage: FC<NoticeDetailPageProps> = () => {
  useScrollToTopAfterMount();

  const navigate = useNavigate();
  const params = useParams();
  const noticeId = params["notice_id"]!;
  const getNoticeDetailQuery = useGetNoticeDetail(noticeId);
  const getMyAccountQuery = useCheckKakaoIdAndLicenseStatus();

  if (getMyAccountQuery.isLoading) {
    return <GlobalLoading visible={true} />;
  }

  if (getMyAccountQuery.isError) {
    // one of exist error
    const error = [
      getMyAccountQuery,
    ].find((query) => query.isError)?.error;
    return <ErrorPage error={error} />;
  }

  if (getNoticeDetailQuery.isLoading) {
    return <GlobalLoading visible={true} />;
  }

  if (getNoticeDetailQuery.isError) {
    // one of exist error
    const error = [
      getNoticeDetailQuery,
      getMyAccountQuery,
    ].find((query) => query.isError)?.error;
    return <ErrorPage error={error} />;
  }

  if (!noticeId) {
    return <div>공지사항이 존재하지 않습니다.</div>;
  }

  const data = getNoticeDetailQuery.data?.data ?? null;

  if (!data) {
    return <div>공지사항이 존재하지 않습니다.</div>;
  }

  const { title, content, created_at } = data;

  return (
    <div className="notice-list-page">
      <NavBar onBack={() => navigate(-1)}>공지사항</NavBar>
      <div className="content px-4 pb-8 overflow-y-scroll">
        <h1
          className="pt-8 pb-3 text-xl font-bold"
          dangerouslySetInnerHTML={{ __html: title }}
        ></h1>
        <div className="text-sm text-zinc-400 mb-8">{created_at}</div>
        <p
          className="text-base"
          dangerouslySetInnerHTML={{ __html: content }}
        ></p>
      </div>
    </div>
  );
};

export default NoticeDetailPage;
