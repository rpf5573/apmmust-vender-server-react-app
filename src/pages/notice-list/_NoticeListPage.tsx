import { List, NavBar, NoticeBar } from "antd-mobile";
import type { FC } from "react";
import { useGetNoticeList } from "./notice-list-api";
import { useNavigate, useParams } from "react-router-dom";
import { Pagination } from "antd";
import GlobalLoading from "@/components/@shared/global-loading/GlobalLoading";
import ErrorPage from "../error/ErrorPage";
import KakaoFloatChatButton from "@/components/@shared/kakao-float-chat-button/KakaoFloatChatButton";
import useCheckKakaoIdAndLicenseStatus from "@/common/hooks/useCheckKakaoIdAndLicenseStatus";

type NoticeListPageProps = any;
const NoticeListPage: FC<NoticeListPageProps> = () => {
  const params = useParams();
  const page = Number(params["page"] ?? 1);

  const navigate = useNavigate();
  const getNoticeListQuery = useGetNoticeList({
    noticeListCountPerPage: 10,
    page,
  });
  const getMyAccountQuery = useCheckKakaoIdAndLicenseStatus();

  if (getMyAccountQuery.isLoading) {
    return <GlobalLoading visible={true} />;
  }

  if (getMyAccountQuery.isError) {
    // one of exist error
    const error = [
      getMyAccountQuery,
    ].find((query) => query.isError)?.error;
    return <ErrorPage error={error} />;
  }

  if (getNoticeListQuery.isLoading) {
    return <GlobalLoading visible={true} />;
  }

  if (getNoticeListQuery.isError) {
    // one of exist error
    const error = [
      getNoticeListQuery,
    ].find((query) => query.isError)?.error;
    return <ErrorPage error={error} />;
  }

  const notice_list = getNoticeListQuery.data?.data.notice_list ?? [];
  const totalCount = getNoticeListQuery.data?.data.total_count ?? 0;

  const handleClickPaginationItem = (page: number) => {
    // set url path like, /product-list/1
    navigate(`/notice-list/${page}`);
  };

  const handleClickNoticeListItem = (noticeId: string) => {
    navigate(`/notice-detail/${noticeId}`);
  };

  return (
    <div className="product-list-page has-pagination">
      <div className="top">
        <NavBar backArrow={false}>공지 사항</NavBar>
        <NoticeBar
          content="최신 데이터를 보려면 새로고침 해주세요"
          color="alert"
        />
        <List className="pb-3 flex-1">
          {notice_list.map((notice) => (
            <List.Item
              key={notice.id}
              onClick={() => handleClickNoticeListItem(`${notice.id}`)}
            >
              <span dangerouslySetInnerHTML={{ __html: notice.title }}></span>
            </List.Item>
          ))}
        </List>
      </div>
      <div className="bottom">
        <div className="flex justify-center">
          <Pagination
            current={page}
            defaultCurrent={1}
            total={totalCount}
            pageSize={10}
            onChange={handleClickPaginationItem}
            showSizeChanger={false}
            disabled={getNoticeListQuery.isLoading}
          />
        </div>
      </div>
      <KakaoFloatChatButton />
    </div>
  );
};

export default NoticeListPage;
