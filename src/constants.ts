export const FABRICS = [
  { label: "면", label_en: "cotton" },
  { label: "폴리에스테르", label_en: "polyester" },
  { label: "나일론", label_en: "nylon" },
  { label: "레이온", label_en: "rayon" },
  { label: "울", label_en: "wool" },
  { label: "아크릴", label_en: "acrylic" },
  { label: "린넨", label_en: "linen" },
  { label: "스판", label_en: "spandex" },
  { label: "폴리우레탄", label_en: "polyurethane" },
  { label: "가죽", label_en: "leather" },
  { label: "캐시미어", label_en: "cashmere" },
  { label: "모달", label_en: "modal" },
  { label: "알파카", label_en: "alpaca" },
  { label: "소가죽", label_en: "cow leather" },
  { label: "양가죽", label_en: "sheep leather" },
  { label: "합성피혁", label_en: "synthetic leather" },
  { label: "펄(퍼)", label_en: "fur" },
];

export const FIELD_NAME = {
  // product
  PRODUCT_ENGLISH_NAME: "product_english_name",
  PRODUCT_KOREAN_NAME: "product_korean_name",
  PRODUCT_CATEGORY_TERM_ID: "product_category_term_id",
  SKU: "sku",
  REGULAR_PRICE: "regular_price",
  SALE_PRICE: "sale_price",
  WEIGHT: "weight",
  STOCK_STATUS: "stock_status",
  SIZE_TERM_ID_LIST: "size_term_id_list",
  COLOUR_TERM_ID_LIST: "colour_term_id_list",
  STYLE_TERM_ID_LIST: "style_term_id_list",
  COMPOSITION_LIST: "composition_list",
  SEASON_TERM_ID: "season_term_id",
  MADE_IN_TERM_ID: "made_in_term_id",
  FEATURED_IMAGE_URL: "featured_image_url",
  GALLERY_IMAGE_URL_LIST: "gallery_image_url_list",
  MIXING_RATE_LIST: "mixing_rate_list",
  LICENSE_FILE_URL: "license_file_url",

  // my account
  EMAIL: "email",
  PASSWORD: "password",
  BRAND_TERM_ID: "brand_term_id",
  BANK: "bank",
  DEPOSIT_ACCOUNT: "deposit_account",
  PHONE_NUMBER: "phone_number",
  BUILDING_TERM_ID: "building_term_id",
  FLOOR: "floor",
  ROOM_NUMBER: "room_number",
  KAKAO_ID: "kakao_id",

  // product list search
  PRODUCT_CATEGORY_TERM_ID_LIST: "product_category_term_id_list",
  START_DATE: "start_date",
  END_DATE: "end_date",

  // signup
  PASSWORD_1: "password_1",
  PASSWORD_2: "password_2",
  BRAND: "brand",
  ACCOUNT_OWNER: "account_owner",
};
