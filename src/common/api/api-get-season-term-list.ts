import axiosInstance from "@/axiosInstance";
import { API, Term } from "../types";
import { AxiosResponse } from "axios";
import { useQuery } from "@tanstack/react-query";

export type ApiGetSeasonTermList = API<unknown, Term[]>;

export const getSeasonTermList = async () => {
  const url = `${import.meta.env.VITE_API_URL}/term/get-season-term-list`;
  const response = await axiosInstance.get<
    ApiGetSeasonTermList["response"],
    AxiosResponse<ApiGetSeasonTermList["response"]>
  >(url, {
    timeout: 1000 * 3,
  });

  return response.data;
};

export const useGetSeasonTermList = () =>
  useQuery<ApiGetSeasonTermList["response"]>({
    queryKey: ["get-season-term-list"],
    queryFn: getSeasonTermList,
    staleTime: 1000 * 60 * 60 * 24, // 24시간 동안 캐싱
    gcTime: 1000 * 60 * 60 * 24, // 24시간 동안 캐싱
  });

export default useGetSeasonTermList;
