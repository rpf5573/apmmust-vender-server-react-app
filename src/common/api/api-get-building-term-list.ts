import axiosInstance from "@/axiosInstance";
import { API, Term } from "../types";
import { AxiosResponse } from "axios";
import { useQuery } from "@tanstack/react-query";

export type ApiGetBuildingTermList = API<unknown, Term[]>;

export const getBuildingTermList = async () => {
  const url = `${import.meta.env.VITE_API_URL}/term/get-building-term-list`;
  const response = await axiosInstance.get<
    ApiGetBuildingTermList["response"],
    AxiosResponse<ApiGetBuildingTermList["response"]>
  >(url, {
    timeout: 1000 * 3,
  });

  return response.data;
};

export const useGetBuildingTermList = () =>
  useQuery<ApiGetBuildingTermList["response"]>({
    queryKey: ["get-building-term-list"],
    queryFn: getBuildingTermList,
    staleTime: 1000 * 60 * 60 * 24, // 24시간 동안 캐싱
    gcTime: 1000 * 60 * 60 * 24, // 24시간 동안 캐싱
  });

export default useGetBuildingTermList;
