import { Card, Statistic } from "antd";
import type { FC } from "react";
import useGetDashboard from "./dashboard-api";
import GlobalLoading from "@/components/@shared/global-loading/GlobalLoading";
import { NoticeBar } from "antd-mobile";
import useScrollToTopAfterMount from "@/common/hooks/useScrollToTopAfterMount";
import KakaoFloatChatButton from "@/components/@shared/kakao-float-chat-button/KakaoFloatChatButton";
import useCheckKakaoIdAndLicenseStatus from "@/common/hooks/useCheckKakaoIdAndLicenseStatus";
import ErrorPage from "../error/ErrorPage";

type DashbardPageProps = any;
const DashbardPage: FC<DashbardPageProps> = () => {
  useScrollToTopAfterMount();

  const getDashboardQuery = useGetDashboard();

  const getMyAccountQuery = useCheckKakaoIdAndLicenseStatus();

  if (getMyAccountQuery.isLoading) {
    return <GlobalLoading visible={true} />;
  }

  if (getMyAccountQuery.isError) {
    // one of exist error
    const error = [
      getMyAccountQuery,
    ].find((query) => query.isError)?.error;
    return <ErrorPage error={error} />;
  }

  if (getDashboardQuery.isLoading) return <GlobalLoading visible={true} />;

  if (getDashboardQuery.isError) return <div>오류가 발생했습니다</div>;

  const data = getDashboardQuery.data?.data;

  if (!data) return <div>데이터가 없습니다</div>;

  const {
    total_income,
    total_order_count,
    total_product_count,
    total_product_instock,
    total_product_outofstock,
    total_income_this_month,
  } = data;

  return (
    <div className="dashboard">
      <NoticeBar
        content="최신 데이터를 보려면 새로고침 해주세요"
        color="alert"
      />
      <div className="flex flex-col">
        <div className="p-2 pb-0">
          <Card bordered={true}>
            <Statistic
              title="총 판매액"
              value={total_income}
              valueStyle={{ color: "rgb(63, 134, 0)" }}
              suffix="원"
            />
          </Card>
        </div>
        <div className="p-2 pb-0">
          <Card bordered={true}>
            <Statistic
              title="총 주문수"
              value={total_order_count}
              valueStyle={{ color: "rgb(207, 19, 34)" }}
              suffix="건"
            />
          </Card>
        </div>
        <div className="p-2 pb-0">
          <Card bordered={true}>
            <Statistic
              title="총 상품수"
              value={total_product_count}
              valueStyle={{ color: "rgb(19, 97, 207)" }}
              suffix="개"
            />
          </Card>
        </div>
        <div className="p-2 pb-0">
          <Card bordered={true}>
            <Statistic
              title="재고 보유 상품수"
              value={total_product_instock}
              valueStyle={{ color: "rgb(19, 97, 207)" }}
              suffix="개"
            />
          </Card>
        </div>
        <div className="p-2 pb-0">
          <Card bordered={true}>
            <Statistic
              title="재고 미보유 상품수"
              value={total_product_outofstock}
              valueStyle={{ color: "rgb(207, 19, 34)" }}
              suffix="개"
            />
          </Card>
        </div>
        <div className="p-2 pb-0">
          <Card bordered={true}>
            <Statistic
              title="이달의 판매액"
              value={total_income_this_month}
              valueStyle={{ color: "rgb(207, 19, 34)" }}
              suffix="원"
            />
          </Card>
        </div>
      </div>
      <KakaoFloatChatButton />
    </div>
  );
};

export default DashbardPage;
