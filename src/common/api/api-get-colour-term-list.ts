import axiosInstance from "@/axiosInstance";
import { API, Term } from "../types";
import { AxiosResponse } from "axios";
import { useQuery } from "@tanstack/react-query";

export type ApiGetColourTermList = API<unknown, Term[]>;

export const getColourTermList = async () => {
  const url = `${import.meta.env.VITE_API_URL}/term/get-colour-term-list`;
  const response = await axiosInstance.get<
    ApiGetColourTermList["response"],
    AxiosResponse<ApiGetColourTermList["response"]>
  >(url, {
    timeout: 1000 * 3,
  });

  return response.data;
};

export const useGetColourTermList = () =>
  useQuery<ApiGetColourTermList["response"]>({
    queryKey: ["get-colour-term-list"],
    queryFn: getColourTermList,
    staleTime: 1000 * 60 * 60 * 24, // 24시간 동안 캐싱
    gcTime: 1000 * 60 * 60 * 24, // 24시간 동안 캐싱
  });

export default useGetColourTermList;
