import axiosInstance from "@/axiosInstance";
import { API, ProductCategoryTerm } from "../types";
import { AxiosResponse } from "axios";
import { useQuery } from "@tanstack/react-query";

export type ApiGetProductCategoryTermTree = API<unknown, ProductCategoryTerm[]>;

export const getProductCategoryTermTree = async () => {
  const url = `${
    import.meta.env.VITE_API_URL
  }/term/get-product-category-term-tree`;
  const response = await axiosInstance.get<
    ApiGetProductCategoryTermTree["response"],
    AxiosResponse<ApiGetProductCategoryTermTree["response"]>
  >(url, {
    timeout: 1000 * 3,
  });

  return response.data;
};

export const useGetProductCategoryTermTree = () =>
  useQuery<ApiGetProductCategoryTermTree["response"]>({
    queryKey: ["get-product-category-term-tree"],
    queryFn: getProductCategoryTermTree,
    staleTime: 1000 * 60 * 60 * 24, // 24시간 동안 캐싱
    gcTime: 1000 * 60 * 60 * 24, // 24시간 동안 캐싱
  });

export default useGetProductCategoryTermTree;
