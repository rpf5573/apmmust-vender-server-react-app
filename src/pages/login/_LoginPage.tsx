import TokenContoller from "@/TokenController";
import { usePostLogin } from "./login-api";
import withAuth from "@/common/hooks/useAuth";
import { Button, Form, Input } from "antd-mobile";
import React from "react";
import { useState } from "react";
import { toast } from "react-hot-toast";
import { useNavigate } from "react-router-dom";
import _ from "underscore";
import { message } from "antd";

const LoginPage: React.FC = () => {
  const { mutateAsync: login } = usePostLogin();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [form] = Form.useForm();
  const navigate = useNavigate();

  const handleSubmit = () => {
    setIsLoading(true);

    const values = form.getFieldsValue();
    const isValid =
      _.isObject(values) && _.has(values, "email") && _.has(values, "password");
    if (!isValid) return;
    const { email, password } = values;

    login({ email, password })
      .then((response) => {
        console.log(response);
        try {
          const userData = response.data;
          TokenContoller.saveUserData(userData);
          message.success("성공적으로 로그인 되었습니다");
          navigate("/notice-list");
        } catch (err) {
          console.log(err);
          toast.error("알수없는 오류가 발생했습니다");
        }
      })
      .catch((err) => {
        const {
          response: { data },
        } = err;
        const errorMessage = data?.message ?? "알수없는 오류가 발생했습니다";
        message.error(errorMessage);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  const handleMoveToSignupPageButton = () => {
    navigate("/signup");
  };

  return (
    <div className="flex flex-col justify-center h-[100vh] pb-40">
      <div className="p-5">
        <img
          src="https://www.apmmust.com/wp-content/uploads/2022/12/apm-Must-320x71.png"
          className="logo dark-logo m-auto"
          alt="apM MUST"
          width="160"
          height="35.5"
        ></img>
      </div>
      <Form
        form={form}
        layout="horizontal"
        footer={
          <>
            <Button
              loading={isLoading}
              block
              color="primary"
              size="large"
              className="mt-1"
              onClick={handleSubmit}
            >
              로그인
            </Button>
            <div className="flex justify-end">
              <Button
                color="primary"
                fill="none"
                onClick={handleMoveToSignupPageButton}
              >
                회원가입
              </Button>
            </div>
          </>
        }
      >
        <Form.Item
          name="email"
          label="이메일"
          rules={[{ required: true, message: "필수 항목입니다" }]}
        >
          <Input placeholder="이메일" />
        </Form.Item>
        <Form.Item
          name="password"
          label="비밀번호"
          rules={[{ required: true, message: "필수 항목입니다" }]}
        >
          <Input placeholder="비밀번호" type="password" />
        </Form.Item>
      </Form>
    </div>
  );
};

export default withAuth(LoginPage);
