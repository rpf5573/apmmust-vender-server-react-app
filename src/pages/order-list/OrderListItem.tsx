import { Order } from "@/common/types";
import { List } from "antd-mobile";
import { useNavigate } from "react-router-dom";
import _ from "underscore";
import { commaizeNumber } from "@toss/utils";

type OrderListItemProps = {
  order: Order;
};

const OrderListItem: React.FC<OrderListItemProps> = ({
  order: {
    order_id: orderId,
    order_number: orderNumber,
    order_date: orderDate,
    country_code_billing: countryCodeBilling,
    customer_name: customerName,
    total
  },
}) => {
  const naviate = useNavigate();

  let description = "";
  if (customerName) {
    description += `주문자명: ${customerName}, `;
  }
  if (countryCodeBilling) {
    description += `국가: ${countryCodeBilling}, `;
  }
  if (orderDate) {
    description += `날짜: ${orderDate}, `;
  }

  description += `총액: ${commaizeNumber(total)}원, `;

  // 끝에서 글자 2개를 삭제한다
  description = description.slice(0, -2);

  const handleListItemClick = () => {
    naviate(`/order-detail/${orderId}`);
  };

  return (
    <List.Item description={description} onClick={handleListItemClick}>
      #{orderNumber}
    </List.Item>
  );
};

export default OrderListItem;
