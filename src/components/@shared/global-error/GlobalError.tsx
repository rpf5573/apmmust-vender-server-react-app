import { Result as AntdResult } from "antd";

const GlobalError: React.FC = (args) => {
  console.dir(args);

  return (
    <div className="flex w-[100vw] h-[var(--full-height-with-bottom)] justify-center items-center">
      <AntdResult
        status="500"
        title="서버 에러"
        subTitle="서버에 문제가 생겼습니다. 관리자에게 문의해주세요"
      />
      ;
    </div>
  );
};

export default GlobalError;
