import axiosInstance from "@/axiosInstance";
import { API, Order } from "@/common/types";
import { useQuery } from "@tanstack/react-query";
import { AxiosResponse } from "axios";

export type ApiGetOrderList = API<
  {
    orderListCountPerPage: number;
    page: number;
  },
  {
    order_list: Order[];
    total_count: number;
    current_page: number;
  }
>;

export const getOrderList = async ({
  orderListCountPerPage,
  page,
}: ApiGetOrderList["params"]) => {
  const url = `${
    import.meta.env.VITE_API_URL
  }/order/get-order-list?page=${page}&order_list_count_per_page=${orderListCountPerPage}`;

  const response = await axiosInstance.get<
    ApiGetOrderList["params"],
    AxiosResponse<ApiGetOrderList["response"]>
  >(url, {
    timeout: 6000,
  });

  return response.data;
};

export const useGetOrderList = ({
  orderListCountPerPage,
  page,
}: ApiGetOrderList["params"]) => {
  const key = ["get-order-list", page, orderListCountPerPage].filter(
    (item) => !!item
  );
  return useQuery<ApiGetOrderList["response"]>({
    queryKey: key,
    queryFn: () => getOrderList({ page, orderListCountPerPage }),
  });
};
