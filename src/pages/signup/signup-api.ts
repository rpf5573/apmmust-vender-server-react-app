import axiosInstance from "@/axiosInstance";
import { API } from "@/common/types";

type ApiPostSignup = API<
  {
    email: string;
    password_1: string;
    password_2: string;
    brand: string;
    building_term_id: string;
    floor: string;
    room_number: string;
    bank: string;
    deposit_account: string;
    phone_number: string;
    account_owner: string;
    style_term_id_list: Array<string>;
    season_term_id: string;
    kakao_id: string;
  },
  unknown
>;

export const postSignup = async (userData: ApiPostSignup["params"]) => {
  const url = `${import.meta.env.VITE_API_URL}/signup`;
  const response = await axiosInstance.post(url, userData, {
    timeout: 30000,
  });
  return response.data;
};
