import { useState, type FC, useEffect } from "react";
import { Controller, useForm, useFormContext } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import useGetSeasonTermList from "@/common/api/api-get-season-term-list";
import useGetStyleTermList from "@/common/api/api-get-style-term-list";
import CustomFormItem from "@/components/@shared/custom-form-item/CustomFormItem";
import { Button, Input, List, NavBar } from "antd-mobile";
import { Term } from "@/common/types";
import { message, Button as AntdButton, UploadFile, Upload } from "antd";
import { FIELD_NAME } from "@/constants";
import { FormProvider } from "react-hook-form";
import TermSelectFormItem from "@/components/@shared/term-select-form-item/TermSelectFormItem";
import _, { isArray } from "underscore";
import "./edit-my-account.scss";
import { editMyAccount } from "./edit-my-account-api";
import GlobalLoading from "@/components/@shared/global-loading/GlobalLoading";
import { useQueryClient } from "@tanstack/react-query";
import useGetMadeInTermList from "@/common/api/api-get-made-in-term-list";
import ErrorPage from "../error/ErrorPage";
import { uploadImage as uploadFile } from "@/common/api/api-s3";
import { UploadOutline } from "antd-mobile-icons";
import useGetMyAccount from "../my-account/my-account-api";

type EditMyAccountProps = any;
const EditMyAccount: FC<EditMyAccountProps> = () => {
  const form = useForm();
  const queryClient = useQueryClient();
  const navigate = useNavigate();
  const [isMyAccountEditing, setIsMyAccountEditing] = useState(false);
  const [isLicenseUploading, setIsLicenseUploading] =
    useState(false);

  const getMyAccountQuery = useGetMyAccount();
  const getSeasonTermListQuery = useGetSeasonTermList();
  const getStyleTermListQuery = useGetStyleTermList();
  const getMadeInTermList = useGetMadeInTermList();

  if (
    getMyAccountQuery.isLoading ||
    getSeasonTermListQuery.isLoading ||
    getStyleTermListQuery.isLoading ||
    getMadeInTermList.isLoading
  ) {
    return <GlobalLoading visible={true} />;
  }

  if (
    getMyAccountQuery.isError ||
    getSeasonTermListQuery.isError ||
    getStyleTermListQuery.isError ||
    getMadeInTermList.isError
  ) {
    // one of exist error
    const error = [
      getMyAccountQuery,
      getSeasonTermListQuery,
      getStyleTermListQuery,
      getMadeInTermList,
    ].find((query) => query.isError)?.error;
    return <ErrorPage error={error} />;
  }

  const myAccount = getMyAccountQuery.data?.data;
  const seasonTermList = getSeasonTermListQuery.data?.data;
  const styleTermList = getStyleTermListQuery.data?.data;
  const madeInTermList = getMadeInTermList.data?.data;

  if (!myAccount || !seasonTermList || !styleTermList || !madeInTermList) {
    const error = new Error(
      "myAccount or seasonTermList or styleTermList or madeInTermList is empty"
    );
    return <ErrorPage error={error} />;
  }

  const {
    email,
    brand_name,
    building,
    floor,
    room_number,
    bank,
    deposit_account,
    account_owner,
    phone_number,
    season_term,
    style_term_list,
    made_in_term,
    kakao_id,
    license_file_url
  } = myAccount;

  const onSubmit = (data: any) => {
    // Extracting other properties
    const password = data[FIELD_NAME.PASSWORD];
    const bank = data[FIELD_NAME.BANK] ?? null;
    const deposit_account = data[FIELD_NAME.DEPOSIT_ACCOUNT] ?? null;
    const account_owner = data[FIELD_NAME.ACCOUNT_OWNER] ?? null;
    const phone_number = data[FIELD_NAME.PHONE_NUMBER] ?? null;
    const style_term_id_list = data[FIELD_NAME.STYLE_TERM_ID_LIST] ?? null;
    const season_term_id = data[FIELD_NAME.SEASON_TERM_ID] ?? null;
    const made_in_term_id = data[FIELD_NAME.MADE_IN_TERM_ID] ?? null;
    const kakao_id = data[FIELD_NAME.KAKAO_ID] ?? null;
    let license_file_url = data[FIELD_NAME.LICENSE_FILE_URL] ?? null;
    if (isArray(license_file_url)) {
      license_file_url = license_file_url[0];
    }

    const myAccount = {
      password,
      bank,
      deposit_account,
      account_owner,
      phone_number,
      style_term_id_list,
      season_term_id,
      made_in_term_id,
      kakao_id,
      license_file_url
    };

    setIsMyAccountEditing(true);

    editMyAccount(myAccount)
      .then(() => {
        queryClient.removeQueries({ queryKey: ["get-my-account"] });
        message.success("내 정보가 수정되었습니다");
        navigate("/my-account");
      })
      .catch((err) => {
        console.error(err);
      
        if (err && err.response && err.response.data) {
          const errorMessage = err.response.data.message;
          message.error(`수정 실패 - ${errorMessage}`);
        } else {
          message.error("수정중 알수없는 오류가 발생했습니다");
        }
      })
      .finally(() => {
        setIsMyAccountEditing(false);
      });
  };

  return (
    <div>
      <NavBar onBack={() => navigate("/my-account")}>내 정보 수정</NavBar>
      <FormProvider {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)} className="adm-form">
          <List>
            {/* 이메일 */}
            <EmailFormItem email={email} />

            {/* 비밀번호 */}
            <PasswordFormItem password={""} />

            {/* 브랜드 */}
            <BrandFormItem brand={brand_name} />

            {/* 스타일 */}
            <StyleFormItem
              defaultTermList={style_term_list ?? []}
              termList={styleTermList}
            />

            {/* 시즌 */}
            <SeasonFormItem
              defaultTerm={season_term}
              termList={seasonTermList}
            />

            {/* 제조국 */}
            <MadeInFormItem
              defaultTerm={made_in_term}
              termList={madeInTermList}
            />

            {/* 은행 */}
            <BankFormItem bank={bank} />

            {/* 계좌번호 */}
            <DepositAccountFormItem depositAccount={deposit_account} />

            {/* 예금주 */}
            <AccountOwnerFormItem accountOwner={account_owner} />

            {/* 매장 폰 번호 */}
            <PhoneNumberFormItem phoneNumber={phone_number} />

            {/* 건물 */}
            <BuildingFormItem building={building} />

            {/* 층 */}
            <FloorFormItem floor={floor} />

            {/* 호수 */}
            <RoomNumberFormItem roomNumber={room_number} />

            {/* 카카오톡 아이디 */}
            <KakaoIdFormItem kakaoId={kakao_id} />

            {/* 사업자 등록증 업로드 */}
            <LicenseUploadFormItem fileUrl={license_file_url} isUploading={isLicenseUploading} setIsUploading={setIsLicenseUploading} />
          </List>
          <div className="adm-form-footer">
            <Button
              block
              type="submit"
              color="primary"
              size="large"
              loading={isMyAccountEditing}
              disabled={isLicenseUploading}
            >
              수정하기
            </Button>
          </div>
        </form>
      </FormProvider>
    </div>
  );
};

export default EditMyAccount;

const EmailFormItem = ({ email }: { email: string }) => {
  const form = useFormContext();

  return (
    <Controller
      name={FIELD_NAME.EMAIL}
      defaultValue={email}
      control={form.control}
      render={({
        field: { ref, ...field },
        fieldState: { invalid, error },
      }) => {
        return (
          <CustomFormItem
            label="이메일"
            showError={invalid}
            errorMessage={error?.message}
          >
            <Input
              placeholder="이메일 입력"
              {...field}
              ref={ref}
              disabled
              defaultValue={email}
              readOnly
            />
          </CustomFormItem>
        );
      }}
      rules={{ required: "이메일을 입력해주세요" }}
    />
  );
};

const PasswordFormItem = ({ password }: { password: string }) => {
  const form = useFormContext();

  return (
    <Controller
      name={FIELD_NAME.PASSWORD}
      defaultValue={password}
      control={form.control}
      render={({
        field: { ref, ...field },
        fieldState: { invalid, error },
      }) => {
        return (
          <CustomFormItem
            label="비밀번호"
            required
            showError={invalid}
            errorMessage={error?.message}
          >
            <Input
              placeholder="비밀번호 입력"
              {...field}
              ref={ref}
              defaultValue={password}
            />
          </CustomFormItem>
        );
      }}
      rules={{ required: "비밀번호를 입력해주세요" }}
    />
  );
};

const BrandFormItem = ({ brand }: { brand: string }) => {
  const form = useFormContext();

  return (
    <Controller
      name={FIELD_NAME.BRAND_TERM_ID}
      defaultValue={brand}
      control={form.control}
      render={({
        field: { ref, ...field },
        fieldState: { invalid, error },
      }) => {
        return (
          <CustomFormItem
            label="브랜드"
            showError={invalid}
            errorMessage={error?.message}
          >
            <Input
              placeholder="브랜드 입력"
              {...field}
              ref={ref}
              disabled
              defaultValue={brand}
              readOnly
            />
          </CustomFormItem>
        );
      }}
      rules={{ required: "브랜드를 입력해주세요" }}
    />
  );
};

const StyleFormItem = ({
  defaultTermList,
  termList,
}: {
  defaultTermList: Term[];
  termList: Term[];
}) => {
  const form = useFormContext();

  return (
    <TermSelectFormItem
      defaultTermList={defaultTermList}
      termList={termList}
      isMultiple={true}
      fieldName={FIELD_NAME.STYLE_TERM_ID_LIST}
      label="스타일"
      form={form}
      required={false}
    />
  );
};

const SeasonFormItem = ({
  defaultTerm,
  termList,
}: {
  defaultTerm: Term | undefined;
  termList: Term[];
}) => {
  const form = useFormContext();

  return (
    <TermSelectFormItem
      defaultTermList={defaultTerm ? [defaultTerm] : []}
      termList={termList}
      isMultiple={false}
      fieldName={FIELD_NAME.SEASON_TERM_ID}
      label="시즌"
      form={form}
      required={false}
    />
  );
};

const MadeInFormItem = ({
  defaultTerm,
  termList,
}: {
  defaultTerm: Term | undefined;
  termList: Term[];
}) => {
  const form = useFormContext();

  return (
    <TermSelectFormItem
      defaultTermList={defaultTerm ? [defaultTerm] : []}
      termList={termList}
      isMultiple={false}
      fieldName={FIELD_NAME.MADE_IN_TERM_ID}
      label="제조국"
      form={form}
      required={false}
    />
  );
};

const BankFormItem = ({ bank }: { bank: string }) => {
  const form = useFormContext();

  return (
    <Controller
      name={FIELD_NAME.BANK}
      defaultValue={bank}
      control={form.control}
      render={({
        field: { ref, ...field },
        fieldState: { invalid, error },
      }) => {
        return (
          <CustomFormItem
            label="은행"
            showError={invalid}
            errorMessage={error?.message}
          >
            <Input
              placeholder="은행 입력"
              {...field}
              ref={ref}
              defaultValue={bank}
            />
          </CustomFormItem>
        );
      }}
    />
  );
};

const DepositAccountFormItem = ({
  depositAccount,
}: {
  depositAccount: string;
}) => {
  const form = useFormContext();

  return (
    <Controller
      name={FIELD_NAME.DEPOSIT_ACCOUNT}
      defaultValue={depositAccount}
      control={form.control}
      render={({
        field: { ref, ...field },
        fieldState: { invalid, error },
      }) => {
        return (
          <CustomFormItem
            label="계좌번호"
            showError={invalid}
            errorMessage={error?.message}
          >
            <Input
              placeholder="계좌번호 입력"
              {...field}
              ref={ref}
              defaultValue={depositAccount}
            />
          </CustomFormItem>
        );
      }}
    />
  );
};

const AccountOwnerFormItem = ({ accountOwner }: { accountOwner: string }) => {
  const form = useFormContext();

  return (
    <Controller
      name={FIELD_NAME.ACCOUNT_OWNER}
      defaultValue={accountOwner}
      control={form.control}
      render={({
        field: { ref, ...field },
        fieldState: { invalid, error },
      }) => {
        return (
          <CustomFormItem
            label="예금주"
            showError={invalid}
            errorMessage={error?.message}
          >
            <Input
              placeholder="예금주 입력"
              {...field}
              ref={ref}
              defaultValue={accountOwner}
            />
          </CustomFormItem>
        );
      }}
    />
  );
};

const PhoneNumberFormItem = ({ phoneNumber }: { phoneNumber: string }) => {
  const form = useFormContext();

  return (
    <Controller
      name={FIELD_NAME.PHONE_NUMBER}
      defaultValue={phoneNumber}
      control={form.control}
      render={({
        field: { ref, ...field },
        fieldState: { invalid, error },
      }) => {
        return (
          <CustomFormItem
            label="매장 폰 번호"
            showError={invalid}
            errorMessage={error?.message}
          >
            <Input
              placeholder="매장 폰 번호 입력"
              {...field}
              ref={ref}
              defaultValue={phoneNumber}
            />
          </CustomFormItem>
        );
      }}
    />
  );
};

const KakaoIdFormItem = ({ kakaoId }: { kakaoId: string }) => {
  const form = useFormContext();

  return (
    <Controller
      name={FIELD_NAME.KAKAO_ID}
      defaultValue={kakaoId}
      control={form.control}
      render={({
        field: { ref, ...field },
        fieldState: { invalid, error },
      }) => {
        return (
          <CustomFormItem
            label="카카오 아이디"
            showError={invalid}
            errorMessage={error?.message}
          >
            <Input
              placeholder="카카오 아이디 입력"
              {...field}
              ref={ref}
              defaultValue={kakaoId}
            />
          </CustomFormItem>
        );
      }}
      rules={{ required: "카카오톡 아이디를 입력해주세요" }}
    />
  );
};

const BuildingFormItem = ({ building }: { building: string }) => {
  const form = useFormContext();

  return (
    <Controller
      name={FIELD_NAME.BUILDING_TERM_ID}
      defaultValue={building}
      control={form.control}
      render={({
        field: { ref, ...field },
        fieldState: { invalid, error },
      }) => {
        return (
          <CustomFormItem
            label="건물"
            showError={invalid}
            errorMessage={error?.message}
          >
            <Input
              placeholder="건물 입력"
              {...field}
              ref={ref}
              defaultValue={building}
              disabled
              readOnly
            />
          </CustomFormItem>
        );
      }}
    />
  );
};

const FloorFormItem = ({ floor }: { floor: string }) => {
  const form = useFormContext();

  return (
    <Controller
      name={FIELD_NAME.FLOOR}
      defaultValue={floor}
      control={form.control}
      render={({
        field: { ref, ...field },
        fieldState: { invalid, error },
      }) => {
        return (
          <CustomFormItem
            label="층"
            showError={invalid}
            errorMessage={error?.message}
          >
            <Input
              placeholder="층 입력"
              {...field}
              ref={ref}
              defaultValue={floor}
              disabled
              readOnly
            />
          </CustomFormItem>
        );
      }}
    />
  );
};

const RoomNumberFormItem = ({ roomNumber }: { roomNumber: string }) => {
  const form = useFormContext();

  return (
    <Controller
      name={FIELD_NAME.ROOM_NUMBER}
      defaultValue={roomNumber}
      control={form.control}
      render={({
        field: { ref, ...field },
        fieldState: { invalid, error },
      }) => {
        return (
          <CustomFormItem
            label="호수"
            showError={invalid}
            errorMessage={error?.message}
          >
            <Input
              placeholder="호수 입력"
              {...field}
              ref={ref}
              defaultValue={roomNumber}
              disabled
              readOnly
            />
          </CustomFormItem>
        );
      }}
    />
  );
};

type LicenseUploadFormItemProps = {
  fileUrl: string;
  isUploading: boolean;
  setIsUploading: React.Dispatch<React.SetStateAction<boolean>>;
};
const LicenseUploadFormItem = ({
  fileUrl,
  isUploading,
  setIsUploading,
}: LicenseUploadFormItemProps) => {
  const form = useFormContext();
  const [selected, setSelected] = useState<Array<UploadFile>>([
    {
      uid: fileUrl,
      name:
        fileUrl.split("/").pop() ??
        fileUrl,
      url: fileUrl,
    },
  ]);

  useEffect(() => {
    form.setValue(
      FIELD_NAME.LICENSE_FILE_URL,
      selected.map((item) => item.url)
    );
    form.clearErrors(FIELD_NAME.LICENSE_FILE_URL);
  }, [selected]);

  async function handleUpload(file: any) {
    try {
      const fileUrl = await uploadFile(file);
      return {
        fileUrl: fileUrl.imageUrl,
        fileKey: fileUrl.imageUrl,
        fileName: file.name,
      };
    } catch (err) {
      message.error("업로드에 실패했습니다. 잠시 후 다시 시도해주세요!");
      throw err;
    }
  }

  return (
    <Controller
      name={FIELD_NAME.LICENSE_FILE_URL}
      control={form.control}
      render={({ fieldState: { invalid, error } }) => (
        <CustomFormItem
          label="사업자 등록증"
          required
          showError={invalid}
          errorMessage={error?.message}
        >
          <Upload
            beforeUpload={(file) => {
              return new Promise((resolve, reject) => {
                // file size limit is 5mb
                if (file.size > 5 * 1024 * 1024) {
                  message.error("이미지 파일은 5MB 이하만 업로드 가능합니다");
                  return reject();
                }
                resolve();
              });
            }}
            customRequest={({ file }) => {
              setIsUploading(true);
              handleUpload(file)
                .then(({ fileUrl, fileKey, fileName }) => {
                  const uploadFile: UploadFile = {
                    uid: fileKey,
                    name: fileName,
                    url: fileUrl,
                    status: "done",
                  };
                  setSelected((prev) => [...prev, uploadFile]);
                })
                .catch(() => {
                  alert(
                    `업로드중 오류가 발생했습니다 - 파일명: ${
                      (file as any).name
                    }`
                  );
                  window.location.reload();
                })
                .finally(() => {
                  setIsUploading(false);
                });
            }}
            fileList={selected}
            maxCount={1}
            multiple={false}
            accept="*/*"
            onChange={({ fileList }) => {
              setSelected(fileList.filter((file) => file.status === "done"));
            }}
            disabled={isUploading}
          >
            <AntdButton
              icon={<UploadOutline />}
              loading={isUploading}
              disabled={isUploading}
            >
              업로드
            </AntdButton>
          </Upload>
        </CustomFormItem>
      )}
      rules={{ required: "사업자 등록증을 선택해주세요" }}
    />
  );
};
