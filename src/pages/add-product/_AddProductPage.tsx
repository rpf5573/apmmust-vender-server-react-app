import React, { useState, type FC, useEffect } from "react";
import { Controller, useForm, useFormContext } from "react-hook-form";
import CustomFormItem from "@/components/@shared/custom-form-item/CustomFormItem";
import {
  Button,
  CascaderView,
  Collapse,
  Input,
  List,
  NavBar,
  NoticeBar,
  Space,
} from "antd-mobile";
import { ProductCategoryTerm, Term } from "@/common/types";
import { InputNumber, Upload, message, Button as AntdButton } from "antd";
import type { UploadFile } from "antd";
import { uploadImage } from "@/common/api/api-s3";
import {
  findTermById,
  transformTermListForCascaderViewOption,
} from "@/common/utils";
import { FIELD_NAME } from "@/constants";
import { FormProvider } from "react-hook-form";
import TermSelectFormItem from "@/components/@shared/term-select-form-item/TermSelectFormItem";
import _ from "underscore";
import "./add-product.scss";
import { addProduct } from "./add-product-api";
import useGetSizeTermList from "@/common/api/api-get-size-term-list";
import useGetColourTermList from "@/common/api/api-get-colour-term-list";
import useGetProductCategoryTermTree from "@/common/api/api-get-product-category-term-tree";
import useGetSeasonTermList from "@/common/api/api-get-season-term-list";
import useGetStyleTermList from "@/common/api/api-get-style-term-list";
import useGetMadeInTermList from "@/common/api/api-get-made-in-term-list";
import { useNavigate } from "react-router-dom";
import GlobalLoading from "@/components/@shared/global-loading/GlobalLoading";
import { UploadOutline } from "antd-mobile-icons";
import { useQueryClient } from "@tanstack/react-query";
import useScrollToTopAfterMount from "@/common/hooks/useScrollToTopAfterMount";
import ErrorPage from "../error/ErrorPage";
import TermSelectFormItemMultipleColumn from "@/components/@shared/term-select-form-item/TermSelectFormItemMultipleColumn";
import { commaizeNumber, decommaizeNumber } from "@toss/utils";
import useCheckKakaoIdAndLicenseStatus from "@/common/hooks/useCheckKakaoIdAndLicenseStatus";
import useTermSelectMultipleColumnsPopup, {
  TermSelectionMultipleColumnsPopup,
} from "@/common/hooks/useTermSelectMultipleCulumnsPopup";
import useGetCompositionTermList from "@/common/api/api-get-composition-term-list";

type AddProductPageProps = any;
const AddProductPage: FC<AddProductPageProps> = () => {
  const form = useForm();
  const navigate = useNavigate();
  const queryClient = useQueryClient();
  const errorCount = Object.keys(form.formState.errors).length;

  useScrollToTopAfterMount();

  const getSizeTermListQuery = useGetSizeTermList();
  const getColourTermListQuery = useGetColourTermList();
  const getProductCategoryTermTreeQuery = useGetProductCategoryTermTree();
  const getSeasonTermListQuery = useGetSeasonTermList();
  const getStyleTermListQuery = useGetStyleTermList();
  const getMadeInTermListQuery = useGetMadeInTermList();
  const getCompositionTermListQuery = useGetCompositionTermList();

  const [isFeaturedImageUploading, setIsFeaturedImageUploading] =
    useState(false);
  const [isGalleryImageUploading, setIsGalleryImageUploading] = useState(false);
  const [isProductAdding, setIsProductAdding] = useState(false);

  const getMyAccountQuery = useCheckKakaoIdAndLicenseStatus();

  if (
    getSizeTermListQuery.isLoading ||
    getColourTermListQuery.isLoading ||
    getProductCategoryTermTreeQuery.isLoading ||
    getSeasonTermListQuery.isLoading ||
    getStyleTermListQuery.isLoading ||
    getMadeInTermListQuery.isLoading ||
    getCompositionTermListQuery.isLoading ||
    getMyAccountQuery.isLoading
  ) {
    return <GlobalLoading visible={true} />;
  }

  if (
    getSizeTermListQuery.isError ||
    getColourTermListQuery.isError ||
    getProductCategoryTermTreeQuery.isError ||
    getSeasonTermListQuery.isError ||
    getStyleTermListQuery.isError ||
    getMadeInTermListQuery.isError ||
    getCompositionTermListQuery.isError ||
    getMyAccountQuery.isError
  ) {
    // one of exist error
    const error = [
      getSizeTermListQuery,
      getColourTermListQuery,
      getProductCategoryTermTreeQuery,
      getSeasonTermListQuery,
      getStyleTermListQuery,
      getMadeInTermListQuery,
      getMyAccountQuery,
    ].find((query) => query.isError)?.error;
    return <ErrorPage error={error} />;
  }

  const sizeTermList = getSizeTermListQuery.data?.data;
  const colourTermList = getColourTermListQuery.data?.data;
  const productCategoryTermTree = getProductCategoryTermTreeQuery.data?.data;
  const seasonTermList = getSeasonTermListQuery.data?.data;
  const styleTermList = getStyleTermListQuery.data?.data;
  const madeInTermList = getMadeInTermListQuery.data?.data;
  const compositionTermList = getCompositionTermListQuery.data?.data;
  const myAccount = getMyAccountQuery.data?.data;

  if (
    !sizeTermList ||
    !colourTermList ||
    !productCategoryTermTree ||
    !seasonTermList ||
    !styleTermList ||
    !madeInTermList ||
    !compositionTermList ||
    !myAccount
  ) {
    return <div>필요한 값이 없습니다</div>;
  }

  const { season_term, style_term_list, made_in_term } = myAccount;

  const onSubmit = (data: any) => {
    if (isProductAdding) return;
    setIsProductAdding(true);
    console.log('data', data);

    // Extracting other properties
    const product_korean_name = data[FIELD_NAME.PRODUCT_KOREAN_NAME]; // 필수
    const product_english_name = data[FIELD_NAME.PRODUCT_ENGLISH_NAME] ?? null; // optional
    const regular_price = data[FIELD_NAME.REGULAR_PRICE]
      ? decommaizeNumber(data[FIELD_NAME.REGULAR_PRICE])
      : 0;
    const sale_price = data[FIELD_NAME.SALE_PRICE]
      ? decommaizeNumber(data[FIELD_NAME.SALE_PRICE])
      : 0;
    const weight = data[FIELD_NAME.WEIGHT] ?? null;
    const product_category_term_id = data[FIELD_NAME.PRODUCT_CATEGORY_TERM_ID];
    const style_term_id_list = data[FIELD_NAME.STYLE_TERM_ID_LIST] ?? null;
    const season_term_id = data[FIELD_NAME.SEASON_TERM_ID] ?? null;
    const featured_image_url =
      _.isArray(data[FIELD_NAME.FEATURED_IMAGE_URL]) &&
      data[FIELD_NAME.FEATURED_IMAGE_URL].length > 0
        ? data[FIELD_NAME.FEATURED_IMAGE_URL][0]
        : null;
    const gallery_image_url_list =
      data[FIELD_NAME.GALLERY_IMAGE_URL_LIST] ?? null;
    const made_in_term_id = data[FIELD_NAME.MADE_IN_TERM_ID] ?? null;
    const size_term_id_list = data[FIELD_NAME.SIZE_TERM_ID_LIST] ?? null;
    const colour_term_id_list = data[FIELD_NAME.COLOUR_TERM_ID_LIST] ?? null;
    let composition_list = data[FIELD_NAME.COMPOSITION_LIST] ?? null;

    // fieldname이 숫자면 좀 이상하니까 slug로 쓰기는 하는데 말이지,,
    // 서버에서 slug를 받아서 term_id로 변환한 다음에 넣는다.
    if (composition_list) {
      composition_list = Object.keys(composition_list).map((slug) => {
        return {
          composition_fabric: slug,
          composition_fabric_rate: Number(composition_list[slug]),
        };
      });
    }

    const product = {
      product_korean_name,
      product_english_name,
      regular_price,
      sale_price,
      weight,
      product_category_term_id,
      style_term_id_list,
      season_term_id,
      featured_image_url,
      gallery_image_url_list,
      made_in_term_id,
      composition_list,
      size_term_id_list,
      colour_term_id_list,
    };

    addProduct(product)
      .then(() => {
        queryClient.removeQueries({ queryKey: ["get-product-list"] });
        message.success("상품이 추가되었습니다");
        navigate("/product-list");
      })
      .catch((err) => {
        console.error(err);
        if (err && err.response && err.response.data) {
          const errorMessage = err.response.data.message;
          message.error(`생성 실패 - ${errorMessage}`);
        } else {
          message.error("상품 생성중 알수없는 오류가 발생했습니다");
        }
      })
      .finally(() => {
        setIsProductAdding(false);
      });
  };

  const isChrome =
    /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);

  return (
    <div className="add-product-page">
      <NavBar backArrow={false}>상품 등록</NavBar>
      {!isChrome && (
        <NoticeBar
          content="크롬 브라우저를 권장합니다"
          color="info"
          closeable
        />
      )}
      <FormProvider {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)} className="adm-form">
          <List>
            {/* 한글 상품명 */}
            <KoreanNameFormItem />

            {/* 영문 상품명 */}
            <EnglishNameFormItem />

            {/* 상품 가격 */}
            <RegularPriceFormItem />

            {/* 할인 가격 */}
            <SalePriceFormItem />

            {/* 상품 카테고리 */}
            <ProductCategoryFormItem termList={productCategoryTermTree} />

            {/* 특성 이미지 */}
            <FeaturedImageFormItem
              isFeaturedImageUploading={isFeaturedImageUploading}
              setIsFeaturedImageUploading={setIsFeaturedImageUploading}
            />

            {/* 갤러리 이미지 */}
            <GalleryImageFormItem
              isGalleryImageUploading={isGalleryImageUploading}
              setIsGalleryImageUploading={setIsGalleryImageUploading}
            />

            {/* 무게 */}
            <WeightFormItem />

            {/* 사이즈 */}
            <SizeFormItem termList={sizeTermList} />

            {/* 색상 */}
            <ColourFormItem termList={colourTermList} />

            {/* 시즌 */}
            <SeasonFormItem
              defaultTerm={season_term}
              termList={seasonTermList}
            />

            {/* 스타일 */}
            <StyleFormItem
              defaultTermList={style_term_list ?? []}
              termList={styleTermList}
            />

            {/* 제조국 */}
            <MadeInFormItem
              defaultTerm={made_in_term}
              termList={madeInTermList}
            />

            {/* 소재(혼용률) */}
            <CompositionFormItem termList={compositionTermList} />
          </List>
          <div className="adm-form-footer">
            <Button
              className="!mb-2"
              block
              type="submit"
              color="primary"
              size="large"
              disabled={
                isFeaturedImageUploading ||
                isGalleryImageUploading ||
                Object.keys(form.formState.errors).length > 0
              }
              loading={
                isProductAdding ||
                isFeaturedImageUploading ||
                isGalleryImageUploading
              }
            >
              생성하기
            </Button>
            {errorCount > 0 && (
              <div className="text-center">
                <span className="text-red-500">필수값을 확인해주세요</span>
              </div>
            )}
          </div>
        </form>
      </FormProvider>
    </div>
  );
};

export default AddProductPage;

const KoreanNameFormItem = () => {
  const form = useFormContext();

  return (
    <Controller
      name={FIELD_NAME.PRODUCT_KOREAN_NAME}
      control={form.control}
      render={({
        field: { ref, ...field },
        fieldState: { invalid, error },
      }) => {
        return (
          <CustomFormItem
            label="한글 상품명"
            required
            showError={invalid}
            errorMessage={error?.message}
          >
            <Input placeholder="상품 이름 입력" {...field} ref={ref} />
          </CustomFormItem>
        );
      }}
      rules={{ required: "상품명을 입력해주세요" }}
    />
  );
};

const EnglishNameFormItem = () => {
  const form = useFormContext();

  return (
    <Controller
      name={FIELD_NAME.PRODUCT_ENGLISH_NAME}
      control={form.control}
      render={({
        field: { ref, ...field },
        fieldState: { invalid, error },
      }) => {
        return (
          <CustomFormItem
            label="영문 상품명"
            showError={invalid}
            errorMessage={error?.message}
          >
            <Input
              placeholder="영문 상품명 입력시 고객에게 노출 될 확률 올라갑니다"
              {...field}
              ref={ref}
            />
          </CustomFormItem>
        );
      }}
    />
  );
};

const RegularPriceFormItem = () => {
  const form = useFormContext();

  return (
    <div className="flex">
      <Controller
        name={FIELD_NAME.REGULAR_PRICE}
        control={form.control}
        render={({ field, fieldState: { invalid, error } }) => (
          <CustomFormItem
            label="가격"
            required
            showError={invalid}
            errorMessage={error?.message}
          >
            <div className="flex flex-col">
              <div className="mb-3">
                <Input
                  placeholder="상품 가격 입력"
                  {...field}
                  onFocus={(e) =>
                    e.target.addEventListener(
                      "wheel",
                      function (e) {
                        e.preventDefault();
                      },
                      { passive: false }
                    )
                  }
                  onChange={(e) => {
                    // check e is number or not, e is string now
                    const num = decommaizeNumber(e);
                    if (isNaN(Number(num))) {
                      field.onChange("");
                      return;
                    }
                    const formattedValue = commaizeNumber(num);
                    field.onChange(
                      formattedValue === "0" ? "" : formattedValue
                    );
                  }}
                />
              </div>
              <MoneyUpBtnGroup
                onMoneyUp={(money) => {
                  const original =
                    field.value && isNaN(Number(field.value))
                      ? decommaizeNumber(field.value)
                      : 0;
                  const total = original + money;
                  field.onChange(commaizeNumber(total));
                }}
              />
            </div>
          </CustomFormItem>
        )}
        rules={{
          required: "가격을 입력해주세요",
        }}
      />
    </div>
  );
};

const SalePriceFormItem = () => {
  const form = useFormContext();

  return (
    <Controller
      name={FIELD_NAME.SALE_PRICE}
      control={form.control}
      render={({ field, fieldState: { invalid, error } }) => (
        <CustomFormItem
          label="할인 가격"
          showError={invalid}
          errorMessage={error?.message}
        >
          <div className="flex flex-col">
            <div className="mb-3">
              <Input
                placeholder="할인된 상품 가격 입력"
                {...field}
                onFocus={(e) =>
                  e.target.addEventListener(
                    "wheel",
                    function (e) {
                      e.preventDefault();
                    },
                    { passive: false }
                  )
                }
                onChange={(e) => {
                  const num = decommaizeNumber(e);
                  if (isNaN(Number(num))) {
                    field.onChange("");
                    return;
                  }
                  const formattedValue = commaizeNumber(num);
                  field.onChange(formattedValue === "0" ? "" : formattedValue);
                }}
              />
            </div>
            <MoneyUpBtnGroup
              onMoneyUp={(money) => {
                const original =
                  field.value && isNaN(Number(field.value))
                    ? decommaizeNumber(field.value)
                    : 0;
                const total = original + money;
                field.onChange(commaizeNumber(total));
              }}
            />
          </div>
        </CustomFormItem>
      )}
    />
  );
};

type MoneyUpBtnGroupProps = {
  onMoneyUp: (money: number) => void;
  className?: string;
};
const MoneyUpBtnGroup = ({ onMoneyUp, className }: MoneyUpBtnGroupProps) => {
  return (
    <Space wrap className={className}>
      <Button
        size="small"
        onClick={() => {
          onMoneyUp(1000);
        }}
      >
        1,000+
      </Button>
      <Button
        size="small"
        onClick={() => {
          onMoneyUp(3000);
        }}
      >
        3,000+
      </Button>
      <Button
        size="small"
        onClick={() => {
          onMoneyUp(5000);
        }}
      >
        5,000+
      </Button>
      <Button
        size="small"
        onClick={() => {
          onMoneyUp(10000);
        }}
      >
        10,000+
      </Button>
    </Space>
  );
};

const FeaturedImageFormItem = ({
  isFeaturedImageUploading,
  setIsFeaturedImageUploading,
}: {
  isFeaturedImageUploading: boolean;
  setIsFeaturedImageUploading: React.Dispatch<React.SetStateAction<boolean>>;
}) => {
  const form = useFormContext();
  const [selected, setSelected] = useState<Array<UploadFile>>([]);

  useEffect(() => {
    form.setValue(
      FIELD_NAME.FEATURED_IMAGE_URL,
      selected.map((item) => item.url)
    );
    form.clearErrors(FIELD_NAME.FEATURED_IMAGE_URL);
  }, [selected]);

  async function handleUpload(file: any) {
    try {
      const imageUrl = await uploadImage(file);
      return imageUrl;
    } catch (err) {
      message.error("이미지 업로드에 실패했습니다. 잠시 후 다시 시도해주세요!");
      throw err;
    }
  }

  return (
    <Controller
      name={FIELD_NAME.FEATURED_IMAGE_URL}
      control={form.control}
      render={({ fieldState: { invalid, error } }) => (
        <CustomFormItem
          label="대표 이미지"
          required
          showError={invalid}
          errorMessage={error?.message}
        >
          <Upload
            beforeUpload={(file) => {
              return new Promise((resolve, reject) => {
                // file size limit is 5mb
                if (file.size > 5 * 1024 * 1024) {
                  message.error("이미지 파일은 5MB 이하만 업로드 가능합니다");
                  return reject();
                }
                resolve();
              });
            }}
            customRequest={({ file }) => {
              setIsFeaturedImageUploading(true);
              handleUpload(file)
                .then(({ imageUrl, imageKey, fileName }) => {
                  const uploadFile: UploadFile = {
                    uid: imageKey,
                    name: fileName,
                    url: imageUrl,
                    status: "done",
                  };
                  setSelected((prev) => [...prev, uploadFile]);
                })
                .catch(() => {
                  alert(
                    `이미지 업로드중 오류가 발생했습니다 - 파일명: ${
                      (file as any).name
                    }`
                  );
                  window.location.reload();
                })
                .finally(() => {
                  setIsFeaturedImageUploading(false);
                });
            }}
            fileList={selected}
            listType="picture"
            maxCount={1}
            multiple={false}
            accept="image/*"
            onChange={({ fileList }) => {
              setSelected(fileList.filter((file) => file.status === "done"));
            }}
            disabled={isFeaturedImageUploading}
          >
            <AntdButton
              icon={<UploadOutline />}
              loading={isFeaturedImageUploading}
              disabled={isFeaturedImageUploading}
            >
              업로드
            </AntdButton>
          </Upload>
        </CustomFormItem>
      )}
      rules={{ required: "대표 이미지를 선택해주세요" }}
    />
  );
};

const GalleryImageFormItem = ({
  isGalleryImageUploading,
  setIsGalleryImageUploading,
}: {
  isGalleryImageUploading: boolean;
  setIsGalleryImageUploading: React.Dispatch<React.SetStateAction<boolean>>;
}) => {
  const maxCount = 30;
  const form = useFormContext();
  const [selected, setSelected] = useState<Array<UploadFile>>([]);
  const [임시파일목록, set임시파일목록] = useState<Array<UploadFile>>([]);
  const [sortedSelected, setSortedSelected] = useState<Array<UploadFile>>([]); // 업로드된 순서대로 정렬된 selected

  useEffect(() => {
    const newSortedSelected = 임시파일목록.reduce((prev, curr) => {
      const found = selected.find((item) => item.name === curr.name);
      if (found && prev.indexOf(found) === -1) {
        prev.push(found);
      }
      return prev;
    }, [] as Array<UploadFile>);
    setSortedSelected(newSortedSelected);
  }, [selected, 임시파일목록]);

  useEffect(() => {
    form.setValue(
      FIELD_NAME.GALLERY_IMAGE_URL_LIST,
      sortedSelected.map((item) => item.url)
    );
    form.clearErrors(FIELD_NAME.GALLERY_IMAGE_URL_LIST);
  }, [sortedSelected]);

  async function handleUpload(file: any) {
    try {
      const imageUrl = await uploadImage(file);
      return imageUrl;
    } catch (err) {
      message.error("이미지 업로드에 실패했습니다");
      throw err;
    }
  }

  return (
    <Controller
      name={FIELD_NAME.GALLERY_IMAGE_URL_LIST}
      control={form.control}
      render={({ fieldState: { invalid, error } }) => (
        <CustomFormItem
          label="갤러리 이미지"
          showError={invalid}
          errorMessage={error?.message}
          className="gallery-image-form-item"
        >
          <Upload
            beforeUpload={(file, fileList) => {
              return new Promise((resolve, reject) => {
                if (fileList.length + selected.length > maxCount) {
                  message.destroy("gallery-image-upload"); // 1개만 보여져야 하기 때문에 이전것은 삭제한다
                  message.error({
                    content: `갤러리 이미지는 ${maxCount}개까지만 업로드 가능합니다`,
                    duration: 5,
                    key: "gallery-image-upload",
                  });
                  return reject();
                }
                // file size limit is 5mb
                if (file.size > 5 * 1024 * 1024) {
                  message.error("이미지 파일은 5MB 이하만 업로드 가능합니다");
                  return reject();
                }
                resolve();
              });
            }}
            // 올린 순서가 아니라 업로드된 순서로 state에 저장된다
            customRequest={({ file }) => {
              setIsGalleryImageUploading(true);
              handleUpload(file)
                .then(({ imageUrl, imageKey, fileName }) => {
                  const uploadFile: UploadFile = {
                    uid: imageKey,
                    name: fileName,
                    url: imageUrl,
                    status: "done",
                  };

                  setSelected((prev) => [...prev, uploadFile]);
                })
                .catch(() => {
                  alert(
                    `이미지 업로드중 오류가 발생했습니다 - 파일명: ${
                      (file as any).name
                    }`
                  );
                  window.location.reload();
                })
                .finally(() => {
                  setIsGalleryImageUploading(false);
                });
            }}
            fileList={sortedSelected}
            listType="picture"
            multiple
            maxCount={maxCount}
            accept="image/*"
            onChange={({ fileList }) => {
              set임시파일목록([...fileList]);
            }}
            disabled={isGalleryImageUploading}
          >
            <AntdButton
              icon={<UploadOutline />}
              loading={isGalleryImageUploading}
              disabled={isGalleryImageUploading}
            >
              업로드 (최대 {maxCount}개)
            </AntdButton>
          </Upload>
        </CustomFormItem>
      )}
    />
  );
};

const WeightFormItem = () => {
  const form = useFormContext();

  return (
    <Controller
      name={FIELD_NAME.WEIGHT}
      control={form.control}
      render={({ field, fieldState: { invalid, error } }) => (
        <CustomFormItem
          label="무게(g)"
          showError={invalid}
          errorMessage={error?.message}
        >
          <Input type="number" placeholder="무게(g) 입력" {...field} />
        </CustomFormItem>
      )}
      rules={{ min: { value: 0, message: "무게는 0g 이상이어야 합니다" } }}
    />
  );
};

const SizeFormItem = ({ termList }: { termList: Term[] }) => {
  const form = useFormContext();

  return (
    <TermSelectFormItemMultipleColumn
      termList={termList}
      isMultiple={true}
      fieldName={FIELD_NAME.SIZE_TERM_ID_LIST}
      label="사이즈"
      form={form}
      required={true}
      errorMessage="사이즈를 선택해주세요"
    />
  );
};

const ColourFormItem = ({ termList }: { termList: Term[] }) => {
  const form = useFormContext();

  return (
    <TermSelectFormItemMultipleColumn
      termList={termList}
      isMultiple={true}
      fieldName={FIELD_NAME.COLOUR_TERM_ID_LIST}
      label="색상"
      form={form}
      required={true}
      errorMessage="색상을 선택해주세요"
    />
  );
};

const SeasonFormItem = ({
  defaultTerm,
  termList,
}: {
  defaultTerm: Term | undefined;
  termList: Term[];
}) => {
  const form = useFormContext();

  return (
    <TermSelectFormItem
      defaultTermList={defaultTerm ? [defaultTerm] : []}
      termList={termList}
      isMultiple={false}
      fieldName={FIELD_NAME.SEASON_TERM_ID}
      label="시즌"
      form={form}
      required={true}
      errorMessage="시즌을 선택해주세요"
    />
  );
};

const StyleFormItem = ({
  defaultTermList,
  termList,
}: {
  defaultTermList: Term[];
  termList: Term[];
}) => {
  const form = useFormContext();

  return (
    <TermSelectFormItem
      defaultTermList={defaultTermList}
      termList={termList}
      isMultiple={true}
      fieldName={FIELD_NAME.STYLE_TERM_ID_LIST}
      label="스타일"
      form={form}
      required={true}
      errorMessage="스타일을 선택해주세요"
    />
  );
};

const MadeInFormItem = ({
  defaultTerm,
  termList,
}: {
  defaultTerm: Term | undefined;
  termList: Term[];
}) => {
  const form = useFormContext();

  return (
    <TermSelectFormItem
      defaultTermList={defaultTerm ? [defaultTerm] : []}
      termList={termList}
      isMultiple={false}
      fieldName={FIELD_NAME.MADE_IN_TERM_ID}
      label="제조국"
      form={form}
      required={true}
      errorMessage="제조국을 선택해주세요"
    />
  );
};

const CompositionFormItem = ({ termList }: { termList: Term[] }) => {
  const form = useFormContext();

  const {
    selected,
    setPopupVisible,
    visible,
    searchText,
    setSearchText,
    oneTerms,
    twoTerms,
    threeTerms,
    handleTermListChange: handlefabricListChange,
    removeItem,
  } = useTermSelectMultipleColumnsPopup({
    defaultTermList: [],
    termList,
    isMultiple: false,
  });

  const handleRemoveItemBtnClick = (fieldName: string) => {
    // find termId with fieldName from termList
    const term = termList.find((term) => `${FIELD_NAME.COMPOSITION_LIST}.${term.slug}` === fieldName);
    term && removeItem(`${term.term_id}`);
    // de-register from form
    form.unregister(fieldName);
  }

  const ListItem = ({
    fieldName,
    label,
  }: {
    fieldName: string;
    label: string;
  }) => (
    <Controller
      name={fieldName}
      control={form.control}
      render={({ field, fieldState: { error } }) => (
        <List.Item
          key={fieldName}
          extra={
            <div className="flex gap-2 items-center">
              <InputNumber min={0} max={100} step={1} addonAfter="%" {...field} />
              <button type="button" className="break-words" onClick={() => handleRemoveItemBtnClick(fieldName)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="red" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-x">
                  <line x1="18" y1="6" x2="6" y2="18"></line>
                  <line x1="6" y1="6" x2="18" y2="18"></line>
                </svg>
              </button>
            </div>
          }
        >
          <span>{label}</span>
          <div className="adm-list-item-description">
            {error && (
              <div className="adm-form-item-feedback-error">
                {error.message ?? "다시 확인해 주세요"}
              </div>
            )}
          </div>
        </List.Item>
      )}
    />
  );

  console.log('selected', selected);

  return (
    <Collapse accordion>
      <Collapse.Panel key="1" title="소재 - 혼합률">
        <List>
          {selected.map((term_id) => {
            const term = findTermById(termList, term_id);
            if (!term) return null;
            return <ListItem key={term?.term_id} fieldName={`${FIELD_NAME.COMPOSITION_LIST}.${term?.slug}`} label={term?.name} />
          })}
          <div className="my-2">
            <Button
              onClick={() => {
                setPopupVisible(true);
              }}
              block
            >
              소재 추가하기
            </Button>
          </div>
        </List>
        <TermSelectionMultipleColumnsPopup
          visible={visible}
          setVisible={setPopupVisible}
          searchText={searchText}
          setSearchText={setSearchText}
          isMultiple={true}
          selected={selected}
          oneTerms={oneTerms}
          twoTerms={twoTerms}
          threeTerms={threeTerms}
          onChange={handlefabricListChange}
        />
      </Collapse.Panel>
    </Collapse>
  );
};

const ProductCategoryFormItem = ({
  termList,
}: {
  termList: ProductCategoryTerm[];
}) => {
  const form = useFormContext();
  const [selected, setSelected] = useState<string[]>([]); // string = TermID, antd cascader는 string만 받는다

  // selected가 바뀌면 form에도 업데이트
  useEffect(() => {
    // form으로 전달 할때는 숫자를 넣어야 하기 때문에, Number로 변환해준다
    form.setValue(
      FIELD_NAME.PRODUCT_CATEGORY_TERM_ID,
      selected.length > 0 ? Number(selected[selected.length - 1]) : null
    );
    form.clearErrors(FIELD_NAME.PRODUCT_CATEGORY_TERM_ID);
  }, [selected]);

  const transformedTermList = transformTermListForCascaderViewOption(termList); // 여기서 termId를 문자열로 변경해 주었다

  return (
    <Controller
      name={FIELD_NAME.PRODUCT_CATEGORY_TERM_ID}
      control={form.control}
      render={({ fieldState: { invalid, error } }) => {
        return (
          <CustomFormItem
            label="상품 카테고리"
            required
            showError={invalid}
            errorMessage={error?.message}
          >
            <CascaderView
              options={transformedTermList}
              value={selected}
              className="product-category-cascader-view"
              onChange={(val) => {
                setSelected(val);
              }}
            />
          </CustomFormItem>
        );
      }}
      rules={{ required: "카테고리를 선택해주세요" }}
    />
  );
};
