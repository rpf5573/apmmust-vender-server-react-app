import axiosInstance from "@/axiosInstance";
import { API, Notice } from "@/common/types";
import { useQuery } from "@tanstack/react-query";
import { AxiosResponse } from "axios";

export type ApiGetNoticeList = API<
  {
    noticeListCountPerPage: number;
    page: number;
  },
  {
    notice_list: Notice[];
    total_count: number;
    current_page: number;
  }
>;

export const getNoticeList = async ({
  noticeListCountPerPage,
  page,
}: ApiGetNoticeList["params"]) => {
  const url = `${
    import.meta.env.VITE_API_URL
  }/notice/get-notice-list?page=${page}&notice_list_count_per_page=${noticeListCountPerPage}`;
  const response = await axiosInstance.get<
    ApiGetNoticeList["params"],
    AxiosResponse<ApiGetNoticeList["response"]>
  >(url, {
    timeout: 5000,
  });

  return response.data;
};

export const useGetNoticeList = ({
  noticeListCountPerPage,
  page,
}: ApiGetNoticeList["params"]) => {
  const key = ["get-notice-list", page, noticeListCountPerPage].filter(
    (item) => !!item
  );
  return useQuery<ApiGetNoticeList["response"]>({
    queryKey: key,
    queryFn: () => getNoticeList({ page, noticeListCountPerPage }),
    staleTime: 1000 * 60 * 10, // 10분 동안 캐싱. 공지사항은 잘 올라오지 않기 때문에 10분이 적당하다
    gcTime: 1000 * 60 * 10, // 10분 동안 캐싱. 공지사항은 잘 올라오지 않기 때문에 10분이 적당하다
  });
};
