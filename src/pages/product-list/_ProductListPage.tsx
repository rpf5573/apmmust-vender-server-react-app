import { useEffect, type FC } from "react";
import { useNavigate } from "react-router-dom";
import { useGetProductList } from "./product-list-api";
import { Button, List, NavBar, SearchBar } from "antd-mobile";
import ProductListItem from "./ProductListItem";
import { Pagination, TreeSelect, DatePicker, Select } from "antd";
import GlobalLoading from "@/components/@shared/global-loading/GlobalLoading";
import useGetProductCategoryTermTree from "@/common/api/api-get-product-category-term-tree";
import { ProductCategoryTerm } from "@/common/types";
import { Controller, useForm } from "react-hook-form";
import { FIELD_NAME } from "@/constants";
import dayjs from "dayjs";
import useScrollToTopAfterMount from "@/common/hooks/useScrollToTopAfterMount";
import ErrorPage from "../error/ErrorPage";
import KakaoFloatChatButton from "@/components/@shared/kakao-float-chat-button/KakaoFloatChatButton";
import useCheckKakaoIdAndLicenseStatus from "@/common/hooks/useCheckKakaoIdAndLicenseStatus";

type ProductListPageProps = any;
const ProductListPage: FC<ProductListPageProps> = () => {
  useScrollToTopAfterMount();

  const navigate = useNavigate();

  const url = new URL(window.location.href);
  const params = new URLSearchParams(url.search);
  const page = Number(params.get("page") ?? 1);
  const product_korean_name = params.get("product_korean_name") ?? "";
  const stock_status = params.get("stock_status") ?? "";
  const start_date = params.get("start_date") ?? "";
  const end_date = params.get("end_date") ?? "";
  const product_category_term_id_list = params.get(
    "product_category_term_id_list"
  )
    ? params.get("product_category_term_id_list")?.split(",")
    : [];
  const productCategoryTermIdListStr = product_category_term_id_list
    ? product_category_term_id_list.join(",")
    : "";

  const form = useForm({
    defaultValues: {
      product_korean_name,
      stock_status,
      start_date: start_date ? dayjs(start_date) : null,
      end_date: end_date ? dayjs(end_date) : null,
      product_category_term_id_list,
    } as any,
  });

  const getProductListQuery = useGetProductList({
    productListCountPerPage: postsPerPage,
    page,
    productKoreanName: product_korean_name,
    stockStatus: stock_status,
    productCategoryTermIdListStr,
    startDate: start_date,
    endDate: end_date,
  });
  const getProductCategoryTermTreeQuery = useGetProductCategoryTermTree();

  const getMyAccountQuery = useCheckKakaoIdAndLicenseStatus();

  useEffect(() => {
    window.localStorage.setItem("latest-product-list-page", String(page));
  }, [page]);

  if (getMyAccountQuery.isLoading) {
    return <GlobalLoading visible={true} />;
  }

  if (getMyAccountQuery.isError) {
    // one of exist error
    const error = [
      getMyAccountQuery,
    ].find((query) => query.isError)?.error;
    return <ErrorPage error={error} />;
  }

  if (
    getProductListQuery.isLoading ||
    getProductCategoryTermTreeQuery.isLoading
  ) {
    return <GlobalLoading visible={true} />;
  }

  if (getProductListQuery.isError || getProductCategoryTermTreeQuery.isError) {
    // one of exist error
    const error = [
      getProductListQuery,
      getProductCategoryTermTreeQuery,
    ].find((query) => query.isError)?.error;
    return <ErrorPage error={error} />;
  }

  const productList = getProductListQuery.data?.data.product_list ?? [];
  const totalCount = getProductListQuery.data?.data.total_count ?? 0;
  const productCategoryTermTree =
    getProductCategoryTermTreeQuery.data?.data ?? [];

  const treeData = transformProductCategoryTermTree(productCategoryTermTree);

  const handleClickPaginationItem = (page: number) => {
    // set url path like, /product-list/1
    navigate(`/product-list/?page=${page}`);
  };

  const handleClearButtonClick = () => {
    form.reset();
    params.delete("product_korean_name");
    params.delete("stock_status");
    params.delete("product_category_term_id_list");
    params.delete("start_date");
    params.delete("end_date");
    navigate({
      pathname: "/product-list",
      search: params.toString(),
    });
  };

  const onSubmit = (data: any) => {
    const {
      product_korean_name,
      stock_status,
      product_category_term_id_list,
      start_date,
      end_date,
    } = data;

    // 초기화
    params.delete("product_korean_name");
    params.delete("stock_status");
    params.delete("product_category_term_id_list");
    params.delete("start_date");
    params.delete("end_date");

    if (product_korean_name) {
      params.set("product_korean_name", product_korean_name);
    }
    if (stock_status) {
      params.set("stock_status", stock_status);
    }
    if (
      product_category_term_id_list &&
      product_category_term_id_list.length > 0
    ) {
      params.set(
        "product_category_term_id_list",
        product_category_term_id_list.join(",")
      );
    }
    if (start_date) {
      params.set("start_date", start_date.format("YYYY-MM-DD"));
    }
    if (end_date) {
      params.set("end_date", end_date.format("YYYY-MM-DD"));
    }

    navigate({
      pathname: "/product-list",
      search: params.toString(),
    });
  };

  return (
    <div className="product-list-page has-pagination">
      <div className="top">
        <NavBar backArrow={false}>상품 목록</NavBar>
        <form
          className="search-panel px-2 py-2"
          onSubmit={form.handleSubmit(onSubmit)}
        >
          <div className="pb-2">
            <div className="flex gap-2">
              <div className="flex-1">
                <Controller
                  name={FIELD_NAME.PRODUCT_KOREAN_NAME}
                  control={form.control}
                  render={({ field: { ref, ...field } }) => {
                    return (
                      <SearchBar
                        placeholder="상품명을 입력해주세요"
                        {...field}
                        ref={ref}
                      />
                    );
                  }}
                />
              </div>
              <div className="grow-0">
                <Controller
                  name={FIELD_NAME.STOCK_STATUS}
                  control={form.control}
                  render={({ field: { ref, ...field } }) => {
                    return (
                      <Select
                        options={[
                          { value: "", label: "재고 무관" },
                          { value: "instock", label: "재고 있음" },
                          { value: "outofstock", label: "재고 없음" },
                        ]}
                        {...field}
                        ref={ref}
                      />
                    );
                  }}
                />
              </div>
            </div>
          </div>
          <div className="product-category-search pb-2">
            <Controller
              name={FIELD_NAME.PRODUCT_CATEGORY_TERM_ID_LIST}
              control={form.control}
              render={({ field: { ref, ...field } }) => {
                return (
                  <TreeSelect
                    showSearch={false}
                    style={{ width: "100%" }}
                    dropdownStyle={{ maxHeight: 400, overflow: "auto" }}
                    placeholder="카테고리 검색"
                    allowClear
                    multiple
                    treeDefaultExpandAll
                    treeData={treeData}
                    ref={ref}
                    value={field.value}
                    onChange={field.onChange}
                  />
                );
              }}
            />
          </div>
          <div className="date-search flex gap-2 pb-2 ">
            <Controller
              name={FIELD_NAME.START_DATE}
              control={form.control}
              render={({ field: { ref, ...field } }) => {
                return (
                  <DatePicker
                    className="flex-1"
                    value={field.value}
                    onChange={(date) => field.onChange(date)}
                    placeholder="시작일"
                  />
                );
              }}
            />
            <Controller
              name={FIELD_NAME.END_DATE}
              control={form.control}
              render={({ field: { ref, ...field } }) => {
                return (
                  <DatePicker
                    className="flex-1"
                    value={field.value}
                    onChange={(date) => field.onChange(date)}
                    placeholder="종료일"
                  />
                );
              }}
            />
          </div>
          <div className="flex gap-2">
            <Button className="flex-1" type="submit" color="success">
              검색
            </Button>
            <Button
              className="flex-1"
              type="button"
              color="warning"
              onClick={handleClearButtonClick}
            >
              초기화
            </Button>
          </div>
        </form>
        <List className="pb-3 flex-1 overflow-y-scroll">
          {productList.map((product) => (
            <ProductListItem key={product.product_id} product={product} />
          ))}
        </List>
      </div>
      <div className="bottom">
        <div className="flex justify-center">
          <Pagination
            current={page}
            defaultCurrent={1}
            total={totalCount}
            pageSize={postsPerPage}
            onChange={handleClickPaginationItem}
            showSizeChanger={false}
            disabled={getProductListQuery.isLoading}
          />
        </div>
      </div>
      <KakaoFloatChatButton />
    </div>
  );
};

const postsPerPage = 10;

export default ProductListPage;

const transformProductCategoryTermTree = (
  productCategoryTermTree: ProductCategoryTerm[]
): any[] => {
  return productCategoryTermTree.map((category) => {
    return {
      title: category.name,
      value: category.term_id,
      children: category.children
        ? transformProductCategoryTermTree(category.children)
        : [],
    };
  });
};
