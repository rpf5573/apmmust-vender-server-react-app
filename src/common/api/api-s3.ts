import { PutObjectCommand, S3Client } from "@aws-sdk/client-s3";
import { generateProductFilename } from "../utils";
import Bugsnag from '@bugsnag/js';

export const bucket = "vendor-apmmust";
export const region = "sgp1";
const config = {
  endpoint: "https://sgp1.digitaloceanspaces.com",
  forcePathStyle: false,
  region,
  credentials: {
    accessKeyId: "DO00YDWHD9DDL42H8JXZ",
    secretAccessKey: "lZMbfz/eiesVVBCnYXLZR7YoRpguW+Q6MnQOwuEzqyw",
  },
};
export const apmmustS3Client = new S3Client(config);
export const getImageUrl = (objectKey: string) => {
  return `https://${bucket}.${region}.digitaloceanspaces.com/${objectKey}`;
};

export const uploadImage = async (file: any) => {
  const key = generateProductFilename(file, "vendor");

  // Step 3: Define the parameters for the object you want to upload.
  const params = {
    Bucket: bucket, // The path to the directory you want to upload the object to, starting with your Space name.
    Key: key, // Object key, referenced whenever you want to access this file later.
    Body: file, // The object's contents. This variable is an object, not a string.
    ACL: "public-read", // Defines ACL permissions, such as private or public.
    Metadata: {},
  };
  try {
    await apmmustS3Client.send(new PutObjectCommand(params));
    const url = getImageUrl(params.Key);
    return { imageUrl: url, imageKey: key, fileName: file.name };
  } catch (err) {
    if (err instanceof Error) {
      Bugsnag.notify(err);
    } else {
      Bugsnag.notify(new Error("업로드 실패"));
      Bugsnag.notify(JSON.stringify(err));
    }
    throw new Error("업로드 실패");
  }
};
