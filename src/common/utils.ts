import { ProductCategoryTerm, Term } from "./types";
import escapeRegExp from "lodash.escaperegexp";

export const generateProductFilename = (file: any, vendor: string) => {
  if (!file || !file.name || !file.uid) {
    throw new Error("잘못된 파일입니다");
  }

  const date = new Date();
  const year = date.getFullYear();
  const month = String(date.getMonth() + 1).padStart(2, "0");
  const day = String(date.getDate()).padStart(2, "0");

  // filename에 space를 _로 대체한다
  const _filename = file.name.replace(/\s/g, "_");

  const filename = `product/${year}/${month}/${day}/${vendor}/${file.uid}__${_filename}`;
  return filename;
};

export const findTermById = (termList: Term[], termId: number | string) => {
  return termList.find((term) => `${term.term_id}` === `${termId}`);
};

export const filterByPrefix = (
  data: Record<string, string>,
  prefix: string
) => {
  return Object.keys(data).reduce((acc: Record<string, string>, key) => {
    if (key.startsWith(prefix)) {
      acc[key] = data[key];
    }
    return acc;
  }, {});
};

export function getOnlyRootPathname(url: string) {
  // Create a new URL object
  const parsedUrl = new URL(url);
  // Get the pathname from the URL
  let pathname = parsedUrl.pathname;
  // Remove leading and trailing slashes
  pathname = pathname.replace(/^\/|\/$/g, "");
  // Split the pathname by '/' and return the first part
  return pathname.split("/")[0];
}

export const is초성match = (query: string, target: string) => {
  const reg = new RegExp(query.split("").map(ch2pattern).join(".*?"), "i");
  const matches = reg.exec(target);
  return Boolean(matches);
};

function ch2pattern(ch: string) {
  const offset = 44032; /* '가'의 코드 */
  // 한국어 음절
  if (/[가-힣]/.test(ch)) {
    const chCode = ch.charCodeAt(0) - offset;
    // 종성이 있으면 문자 그대로를 찾는다.
    if (chCode % 28 > 0) {
      return ch;
    }

    const begin = Math.floor(chCode / 28) * 28 + offset;
    const end = begin + 27;
    return `[\\u${begin.toString(16)}-\\u${end.toString(16)}]`;
  }

  // 한글 자음
  if (/[ㄱ-ㅎ]/.test(ch)) {
    const con2syl = {
      ㄱ: "가".charCodeAt(0),
      ㄲ: "까".charCodeAt(0),
      ㄴ: "나".charCodeAt(0),
      ㄷ: "다".charCodeAt(0),
      ㄸ: "따".charCodeAt(0),
      ㄹ: "라".charCodeAt(0),
      ㅁ: "마".charCodeAt(0),
      ㅂ: "바".charCodeAt(0),
      ㅃ: "빠".charCodeAt(0),
      ㅅ: "사".charCodeAt(0),
    } as Record<string, number>;
    const begin =
      con2syl[ch] ||
      (ch.charCodeAt(0) - 12613) /* 'ㅅ'의 코드 */ * 588 + con2syl["ㅅ"];
    const end = begin + 587;
    return `[${ch}\\u${begin.toString(16)}-\\u${end.toString(16)}]`;
  }

  // 그 외엔 그대로 내보냄
  // escapeRegExp는 lodash에서 가져옴
  return escapeRegExp(ch);
}

export const transformTermListForCascaderViewOption = (terms: ProductCategoryTerm[]): any[] => {
  // Recursive function to transform ProductCategoryTerms for Cascader view
  const transform = (term: ProductCategoryTerm): any => {
    return {
      label: term.name,
      value: `${term.term_id}`,
      children: term.children && term.children.length > 0 
        ? term.children.map(transform) 
        : null
    };
  };

  return terms.map(transform);
}

export const findProductCategoryTermAncestors = (termId: number, terms: ProductCategoryTerm[]): number[] => {
  const findTerm = (id: number, terms: ProductCategoryTerm[]): ProductCategoryTerm | undefined => {
    for (const term of terms) {
      if (term.term_id === id) return term;
      if (term.children) {
        const found = findTerm(id, term.children);
        if (found) return found;
      }
    }
    return undefined;
  };

  const buildAncestorList = (term: ProductCategoryTerm | undefined, ancestors: number[] = []): number[] => {
    if (!term) return ancestors;
    ancestors.unshift(term.term_id);
    return buildAncestorList(findTerm(term.parent, terms), ancestors);
  };

  return buildAncestorList(findTerm(termId, terms));
};
