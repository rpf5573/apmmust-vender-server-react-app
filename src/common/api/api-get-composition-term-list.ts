import axiosInstance from "@/axiosInstance";
import { API, Term } from "../types";
import { AxiosResponse } from "axios";
import { useQuery } from "@tanstack/react-query";

export type ApiGetCompositionTermList = API<unknown, Term[]>;

export const getCompositionTermList = async () => {
  const url = `${import.meta.env.VITE_API_URL}/term/get-composition-term-list`;
  const response = await axiosInstance.get<
    ApiGetCompositionTermList["response"],
    AxiosResponse<ApiGetCompositionTermList["response"]>
  >(url, {
    timeout: 1000 * 3,
  });

  return response.data;
};

export const useGetCompositionTermList = () =>
  useQuery<ApiGetCompositionTermList["response"]>({
    queryKey: ["get-composition-term-list"],
    queryFn: getCompositionTermList,
    staleTime: 1000 * 60 * 60 * 24, // 24시간 동안 캐싱
    gcTime: 1000 * 60 * 60 * 24, // 24시간 동안 캐싱
  });

export default useGetCompositionTermList;
