import axiosInstance from "@/axiosInstance";
import { API, Product } from "@/common/types";
import { useQuery } from "@tanstack/react-query";
import { AxiosResponse } from "axios";

export type ApiGetProductDetail = API<unknown, Product>;

// get product
export const getProductDetail = async (product_id: string) => {
  const response = await axiosInstance.get<ApiGetProductDetail["response"]>(
    `${
      import.meta.env.VITE_API_URL
    }/product/get-product-detail?product_id=${product_id}`,
    {
      timeout: 4000,
    }
  );

  return response.data;
};

export const useGetProductDetail = (product_id: string) =>
  useQuery<ApiGetProductDetail["response"]>({
    queryKey: ["get-product-detail", product_id],
    queryFn: () => getProductDetail(product_id),
    staleTime: 1000 * 60 * 60 * 24, // 24시간 동안 캐싱
    gcTime: 1000 * 60 * 60 * 24, // 24시간 동안 캐싱
  });

export default useGetProductDetail;

export type ApiDeleteProduct = API<
  unknown,
  {
    code: string;
    data: Product;
    message: string;
  }
>;

// delete product
export const deleteProduct = async (product_id: string | number) => {
  const url = `${
    import.meta.env.VITE_API_URL
  }/product/delete-product?product_id=${product_id}`;
  const response = await axiosInstance.delete<
    ApiDeleteProduct["response"],
    AxiosResponse<ApiDeleteProduct["response"]>
  >(url);

  return response.data;
};
