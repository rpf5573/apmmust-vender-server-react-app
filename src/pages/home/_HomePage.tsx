import { atom, useAtomValue } from "jotai";
import { type FC } from "react";
import GlobalLoading from "@components/@shared/global-loading/GlobalLoading";
import { Route, Routes, useNavigate } from "react-router-dom";
import {
  AddOutline,
  AppstoreOutline,
  BellOutline,
  FolderOutline,
  UnorderedListOutline,
  UserOutline,
} from "antd-mobile-icons";
import ProductListPage from "@pages/product-list/_ProductListPage";
import ProductDetailPage from "@pages/product-detail/_ProductDetailPage";
import AddProductPage from "@pages/add-product/_AddProductPage";
import EditProductPage from "@pages/edit-product/_EditProductPage";
import OrderListPage from "@pages/order-list/_OrderListPage";
import NoticeListPage from "@pages/notice-list/_NoticeListPage";
import MyAccountPage from "@pages/my-account/_MyAccountPage";
import { TabBar } from "antd-mobile";
import withAuth from "@/common/hooks/useAuth";
import OrderDetailPage from "../order-detail/_OrderDetailPage";
import EditMyAccount from "../edit-my-account/_EditMyAccount";
import NoticeDetailPage from "../notice-detail/_NoticeDetailPage";
import DashbardPage from "../dashboard/_DashbardPage";
import { getOnlyRootPathname } from "@/common/utils";

type HomePageProps = any;
const HomePage: FC<HomePageProps> = () => {
  const isGlobalLoading = useAtomValue(atom_showGlobalLoading);
  const navigate = useNavigate();
  const rootPathname = getOnlyRootPathname(window.location.href);

  const handleChange = (path: string) => {
    navigate(path);
  };

  return (
    <>
      <GlobalLoading visible={isGlobalLoading} />
      <div className="flex flex-col min-h-[100vh] pb-[42px]">
        <div className="pb-3 flex flex-col flex-1">
          <Routes>
            <Route path="product-list" element={<ProductListPage />} />
            <Route path="product-list/*" element={<ProductListPage />} />
            <Route
              path="product-detail"
              element={<div>상품 아이디를 정해주세요</div>}
            />
            <Route
              path="product-detail/:product_id"
              element={<ProductDetailPage />}
            />
            <Route path="add-product" element={<AddProductPage />} />
            <Route
              path="edit-product/:product_id"
              element={<EditProductPage />}
            />

            <Route path="order-list" element={<OrderListPage />} />
            <Route path="order-list/:page" element={<OrderListPage />} />

            <Route
              path="order-detail"
              element={<div>주문 아이디를 정해주세요</div>}
            />
            <Route
              path="order-detail/:order_id"
              element={<OrderDetailPage />}
            />

            <Route path="notice-list" element={<NoticeListPage />} />
            <Route path="notice-list/:page" element={<NoticeListPage />} />

            <Route
              path="notice-detail"
              element={<div>공지사항 게시글 아이디를 정해주세요</div>}
            />
            <Route
              path="notice-detail/:notice_id"
              element={<NoticeDetailPage />}
            />

            <Route path="my-account" element={<MyAccountPage />} />
            <Route path="edit-my-account" element={<EditMyAccount />} />

            <Route path="dashboard" element={<DashbardPage />} />

            <Route path="*" element={<ProductListPage />} />
          </Routes>
        </div>
        <div className="z-100 bg-white shadow-[0px_0px_20px_-15px_black] fixed left-0 right-0 bottom-0">
          <TabBar activeKey={rootPathname} onChange={handleChange}>
            {tabs.map((item) => (
              <TabBar.Item key={item.key} icon={item.icon} title={item.title} />
            ))}
          </TabBar>
        </div>
      </div>
    </>
  );
};

export const atom_showGlobalLoading = atom(false);

const tabs = [
  {
    key: "product-list",
    title: "상품목록",
    icon: <UnorderedListOutline />,
  },
  {
    key: "add-product",
    title: "상품등록",
    icon: <AddOutline />,
  },
  {
    key: "order-list",
    title: "주문목록",
    icon: <FolderOutline />,
  },
  {
    key: "notice-list",
    title: "공지사항",
    icon: <BellOutline />,
  },
  {
    key: "dashboard",
    title: "대쉬보드",
    icon: <AppstoreOutline />,
  },
  {
    key: "my-account",
    title: "마이페이지",
    icon: <UserOutline />,
  },
];

export default withAuth(HomePage);
