import { Product } from "@/common/types";
import { Avatar, Button, Dialog, List } from "antd-mobile";
import { useRef, useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  editProductModifiedDateToNow,
  editProductStockStatus,
} from "./product-list-api";
import { message } from "antd";
import { useQueryClient } from "@tanstack/react-query";
import { DialogShowHandler } from "antd-mobile/es/components/dialog";
import { commaizeNumber } from '@toss/utils';

type ProductListItemProps = {
  product: Product;
};

const ProductListItem: React.FC<ProductListItemProps> = ({ product }) => {
  const queryClient = useQueryClient();
  const [끌어올리기작동중, set끌어올리기작동중] = useState(false);
  const [품절처리하기작동중, set품절처리하기작동중] = useState(false);
  const [품절처리취소작동중, set품절처리취소작동중] = useState(false);
  const handler = useRef<DialogShowHandler>();

  const {
    product_id,
    product_name,
    product_korean_name,
    regular_price,
    sale_price,
    product_category_term: { name: product_category_term_name },
    stock_status: stock_status_en,
    created_at,
  } = product;
  const stock_status = stock_status_en === "instock" ? "재고있음" : "품절";

  const naviate = useNavigate();

  const handleListItemClick = () => {
    naviate(`/product-detail/${product_id}`);
  };

  const 끌어올리기버튼핸들러 = (productId: string) => {
    set끌어올리기작동중(true);

    editProductModifiedDateToNow(productId)
      .then(() => {
        queryClient.refetchQueries({ queryKey: ["get-product-list"] });
        message.success("끌어올리기 성공");
      })
      .catch((err) => {
        console.error(err);

        message.error(err.message);
      })
      .finally(() => {
        set끌어올리기작동중(false);
      });
  };

  const 품절처리 = (productId: string) => {
    set품절처리하기작동중(true);

    editProductStockStatus({
      product_id: productId,
      stock_status: "outofstock",
    })
      .then(() => {
        queryClient.refetchQueries({ queryKey: ["get-product-list"] });
        message.success("품절처리 성공");
      })
      .catch((err) => {
        console.error(err);

        message.error(err.message);
      })
      .finally(() => {
        set품절처리하기작동중(false);
      });
  };

  const 품절처리확인모달오픈핸들러 = (productId: string) => {
    handler.current = Dialog.show({
      title: "정말 품절 시키겠습니까?",
      content: "확인을 누르시면 품절처리됩니다",
      actions: [
        [
          {
            key: "cancel",
            text: "취소",
          },
          {
            key: "yes",
            text: "확인",
            bold: true,
            danger: true,
          },
        ],
      ],
      closeOnMaskClick: true,
      onAction: (action: any) => {
        if (action.key === "yes") {
          handler.current?.close();
          품절처리(productId);
        }
        if (action.key === "cancel") {
          handler.current?.close();
        }
      },
    });
  };

  const 품절처리취소 = (productId: string) => {
    set품절처리취소작동중(true);

    editProductStockStatus({
      product_id: productId,
      stock_status: "instock",
    })
      .then(() => {
        queryClient.refetchQueries({ queryKey: ["get-product-list"] });
        message.success("품절처리취소 성공");
      })
      .catch((err) => {
        console.error(err);
        
        message.error(err.message);
      })
      .finally(() => {
        set품절처리취소작동중(false);
      });
  };

  const 품절처리취소확인모달오픈핸들러 = (productId: string) => {
    handler.current = Dialog.show({
      title: "품절처리 취소하시겠습니까?",
      content: "확인을 누르시면 재고있음으로 변경됩니다",
      actions: [
        [
          {
            key: "cancel",
            text: "취소",
          },
          {
            key: "yes",
            text: "확인",
            bold: true,
            danger: true,
          },
        ],
      ],
      closeOnMaskClick: true,
      onAction: (action: any) => {
        if (action.key === "yes") {
          handler.current?.close();
          품절처리취소(productId);
        }
        if (action.key === "cancel") {
          handler.current?.close();
        }
      },
    });
  };

  return (
    <List.Item
      prefix={<Avatar src={product.featured_image_url} />}
      description={
        <div>
          <div>
            {`일반가격: ${commaizeNumber(regular_price)}원 / 할인가격: ${commaizeNumber(sale_price)}원`}
          </div>
          <div>
            {`카테고리: ${product_category_term_name} / 재고상태: ${stock_status}`}
          </div>
          <div className="mb-2">
            {`등록날짜: ${created_at}`}
          </div>
          <div className="flex gap-2">
            {product.stock_status === "instock" && (
              <Button
                size="mini"
                color="danger"
                onClick={(e) => {
                  e.stopPropagation();
                  품절처리확인모달오픈핸들러(product.product_id);
                }}
                loading={품절처리하기작동중}
                disabled={품절처리하기작동중}
              >
                품절처리하기
              </Button>
            )}
            {product.stock_status === "outofstock" && (
              <Button
                size="mini"
                color="success"
                onClick={(e) => {
                  e.stopPropagation();
                  품절처리취소확인모달오픈핸들러(product.product_id);
                }}
                loading={품절처리취소작동중}
                disabled={품절처리취소작동중}
              >
                품절처리취소
              </Button>
            )}
            <Button
              size="mini"
              color="primary"
              onClick={(e) => {
                e.stopPropagation();
                끌어올리기버튼핸들러(product.product_id);
              }}
              loading={끌어올리기작동중}
              disabled={끌어올리기작동중}
            >
              끌어올리기
            </Button>
          </div>
        </div>
      }
      onClick={handleListItemClick}
    >
      <b>{product_korean_name ? product_korean_name : product_name}</b>
    </List.Item>
  );
};

export default ProductListItem;
