// 색상, 사이즈 선택시에만 사용됩니다

import { Term } from "@/common/types";
import { Controller, type UseFormReturn } from "react-hook-form";
import CustomFormItem from "../custom-form-item/CustomFormItem";
import { Button, Space, Tag } from "antd-mobile";
import { findTermById } from "@/common/utils";
import useTermSelectMultipleColumnsPopup, { TermSelectionMultipleColumnsPopup } from "@/common/hooks/useTermSelectMultipleCulumnsPopup";
import { useEffect } from "react";
import _ from "underscore";

type TermSelectFormItemMultipleColumnProps = {
  defaultTermList?: Term[];
  termList: Term[];
  isMultiple: boolean;
  fieldName: string;
  label: string;
  form: UseFormReturn;
  required?: boolean;
  errorMessage?: string;
  disabled?: boolean;
};

const TermSelectFormItemMultipleColumn = ({
  defaultTermList,
  termList,
  isMultiple,
  fieldName,
  label,
  form,
  required,
  errorMessage,
  disabled,
}: TermSelectFormItemMultipleColumnProps) => {
  const {
    selected,
    setPopupVisible,
    visible,
    searchText,
    setSearchText,
    oneTerms,
    twoTerms,
    threeTerms,
    handleTermListChange,
  } = useTermSelectMultipleColumnsPopup({
    defaultTermList,
    termList,
    isMultiple,
  });

    // selected가 바뀌면 form에도 업데이트
    useEffect(() => {
      form.setValue(fieldName, isMultiple ? selected : selected[0]);
      if (!_.isEmpty(selected)) {
        form.clearErrors(fieldName);
      }
    }, [selected]);

  return (
    <Controller
      name={fieldName}
      control={form.control}
      render={({ fieldState: { invalid, error } }) => (
        <CustomFormItem
          label={label}
          required={required}
          showError={invalid}
          errorMessage={error?.message}
        >
          <Space align="center">
            <Button
              onClick={() => {
                setPopupVisible(true);
              }}
              disabled={disabled}
            >
              선택
            </Button>
            <div className="flex gap-x-1">
              {selected.map((term_id) => (
                <Tag
                  key={term_id}
                  color="primary"
                  fill="outline"
                  className="!text-[16px]"
                >
                  {findTermById(termList, term_id)?.name.split("_")[0]}
                </Tag>
              ))}
            </div>
          </Space>
          <TermSelectionMultipleColumnsPopup 
            visible={visible} 
            setVisible={setPopupVisible} 
            searchText={searchText} 
            setSearchText={setSearchText} 
            isMultiple={isMultiple} 
            selected={selected} 
            oneTerms={oneTerms} 
            twoTerms={twoTerms} 
            threeTerms={threeTerms} 
            onChange={handleTermListChange} />
        </CustomFormItem>
      )}
      rules={{ required: errorMessage }}
    />
  );
};

export default TermSelectFormItemMultipleColumn;
