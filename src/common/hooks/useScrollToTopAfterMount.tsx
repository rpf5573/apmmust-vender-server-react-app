import { useEffect } from "react";

const useScrollToTopAfterMount = () => {
  useEffect(() => {
    window.scrollTo(0, 0)
  }, []);
};

export default useScrollToTopAfterMount;