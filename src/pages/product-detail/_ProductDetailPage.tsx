import { Button, Dialog, Form, Input, NavBar, Swiper } from "antd-mobile";
import { useRef, type FC, useMemo, useState } from "react";
import { message } from "antd";
import { useGetProductDetail } from "./product-detail-api";
import { DialogShowHandler } from "antd-mobile/es/components/dialog";
import { useNavigate, useParams } from "react-router-dom";
import { deleteProduct } from "./product-detail-api";
import { Term } from "@/common/types";
import _ from "underscore";
import GlobalLoading from "@/components/@shared/global-loading/GlobalLoading";
import useScrollToTopAfterMount from "@/common/hooks/useScrollToTopAfterMount";
import ErrorPage from "../error/ErrorPage";
import { useQueryClient } from "@tanstack/react-query";
import { commaizeNumber } from "@toss/utils";
import useCheckKakaoIdAndLicenseStatus from "@/common/hooks/useCheckKakaoIdAndLicenseStatus";

type ProductDetailPageProps = any;
const ProductDetailPage: FC<ProductDetailPageProps> = () => {
  useScrollToTopAfterMount();
  const queryClient = useQueryClient();
  const params = useParams();
  const productId = params["product_id"]!; // 이렇게 있다고 가정해도 되는걸까?
  const navigate = useNavigate();
  const handler = useRef<DialogShowHandler>();
  const getProductDetailQuery = useGetProductDetail(productId);
  const latestProductListPage = useMemo(() => {
    const page = window.localStorage.getItem("latest-product-list-page");
    return page ?? 1;
  }, []);
  const [showGlobalLoading, setShowGlobalLoading] = useState(false);

  const getMyAccountQuery = useCheckKakaoIdAndLicenseStatus();

  if (getMyAccountQuery.isLoading) {
    return <GlobalLoading visible={true} />;
  }

  if (getMyAccountQuery.isError) {
    // one of exist error
    const error = [
      getMyAccountQuery,
    ].find((query) => query.isError)?.error;
    return <ErrorPage error={error} />;
  }

  if (getProductDetailQuery.isLoading) {
    return <GlobalLoading visible={true} />;
  }

  if (getProductDetailQuery.isError) {
    // one of exist error
    const error = [
      getProductDetailQuery,
    ].find((query) => query.isError)?.error;
    return <ErrorPage error={error} />;
  }

  const product = getProductDetailQuery.data?.data ?? null;
  if (!product) {
    return <div>상품이 존재하지 않습니다.</div>;
  }

  if (!product.featured_image_url) {
    return <div>상품 이미지가 존재하지 않습니다.</div>;
  }

  if (
    !product.gallery_image_url_list ||
    !Array.isArray(product.gallery_image_url_list)
  ) {
    return <div>상품 이미지가 존재하지 않습니다.</div>;
  }

  console.log('product', product);

  const imageUrls = [
    product.featured_image_url,
    ...product.gallery_image_url_list,
  ];

  const handleClickEditButton = () => {
    navigate(`/edit-product/${product.product_id}`);
  };

  const handleClickDeleteButton = () => {
    handler.current = Dialog.show({
      title: "상품을 삭제하시겠습니까?",
      content: "삭제된 상품은 복구될 수 없습니다",
      actions: [
        [
          {
            key: "cancel",
            text: "취소",
          },
          {
            key: "delete",
            text: "삭제하기",
            bold: true,
            danger: true,
          },
        ],
      ],
      closeOnMaskClick: true,
      onAction: (action: any) => {
        if (action.key === "delete") {
          handler.current?.close();
          setShowGlobalLoading(true);
          deleteProduct(product.product_id)
            .then((res) => {
              message.success(res.message);
              queryClient.removeQueries({
                queryKey: ["get-product-list"],
              });
              setTimeout(() => {
                navigate("/product-list");
              }, 500);
            })
            .catch((err) => {
              console.error(err);

              message.error(err.message);
              queryClient.removeQueries({
                queryKey: ["get-product-list"],
              });
              setTimeout(() => {
                navigate("/product-list");
              }, 500);
            }).finally(() => {
              setShowGlobalLoading(false);
            });
        }
        if (action.key === "cancel") {
          handler.current?.close();
        }
      },
    });
  };

  if (showGlobalLoading) {
    return <GlobalLoading visible={true} />;
  }

  return (
    <div className="product-item-detail">
      <NavBar
        onBack={() => navigate(`/product-list/?page=${latestProductListPage}`)}
      >
        상품 상세
      </NavBar>
      <Swiper className="image-slider" autoplay={false} loop={true}>
        {imageUrls.map((url, index) => (
          <Swiper.Item key={index}>
            <img className="w-[60vw] ml-auto mr-auto" src={url} />
          </Swiper.Item>
        ))}
      </Swiper>
      <Form className="mb-4">
        <Form.Item label="한글 상품명">
          <Input value={product.product_korean_name} readOnly />
        </Form.Item>
        <Form.Item label="영문 상품명">
          <Input value={product.product_name} readOnly />
        </Form.Item>
        <Form.Item label="가격">
          <Input value={`${commaizeNumber(product.regular_price)}`} readOnly />
        </Form.Item>
        {_.isNumber(product.sale_price) && (
          <Form.Item label="할인 가격">
            <Input value={`${commaizeNumber(product.sale_price)}`} readOnly />
          </Form.Item>
        )}
        {_.isNumber(product.weight) && (
          <Form.Item label="무게">
            <Input value={`${product.weight}`} readOnly />
          </Form.Item>
        )}
        {_.isArray(product.size_term_list) && (
          <Form.Item label="사이즈">
            <Input value={termArrayToString(product.size_term_list)} readOnly />
          </Form.Item>
        )}
        {_.isArray(product.colour_term_list) && (
          <Form.Item label="색상">
            <div className="adm-input">
              {termArrayToString(product.colour_term_list)}
            </div>
          </Form.Item>
        )}
        {_.isObject(product.gender_term) && (
          <Form.Item label="성별">
            <Input value={product.gender_term.name} readOnly />
          </Form.Item>
        )}
        {_.isObject(product.product_category_term) && (
          <Form.Item label="카테고리">
            <Input value={product.product_category_term.name} readOnly />
          </Form.Item>
        )}
        {_.isObject(product.season_term) && (
          <Form.Item label="시즌">
            <Input value={product.season_term.name} readOnly />
          </Form.Item>
        )}
        {_.isArray(product.style_term_list) && (
          <Form.Item label="스타일">
            <Input
              value={termArrayToString(product.style_term_list)}
              readOnly
            />
          </Form.Item>
        )}
        {_.isObject(product.made_in_term) && (
          <Form.Item label="제조국">
            <Input value={product.made_in_term.name} readOnly />
          </Form.Item>
        )}
        {_.isArray(product.composition_list) && (
          <Form.Item label="소재 - 혼용률">
            <div className="adm-input">
            {product.composition_list.map(item => `${item.composition_fabric.name}: ${item.composition_fabric_rate}%`).join(' / ')}
            </div>
          </Form.Item>
        )}
        <Form.Item label="SKU">
          <Input value={product.sku} readOnly />
        </Form.Item>
      </Form>

      <div className="product-actions flex gap-3 px-3 py-3">
        <Button
          block
          color="danger"
          size="large"
          onClick={handleClickDeleteButton}
        >
          삭제하기
        </Button>
        <Button
          block
          color="primary"
          size="large"
          onClick={handleClickEditButton}
        >
          수정하기
        </Button>
      </div>
    </div>
  );
};

export default ProductDetailPage;

const termArrayToString = (terms: Array<Term>) => {
  return terms
    .reduce((acc, cur) => {
      return `${acc}, ${cur.name}`;
    }, "")
    .substring(2);
};
