import { List, NavBar, NoticeBar } from "antd-mobile";
import type { FC } from "react";
import { useNavigate, useParams } from "react-router-dom";
import "./order-item-detail.scss";
import useGetOrderDetail from "./order-detail-api";
import { OrderItem } from "@/common/types";
import GlobalLoading from "@/components/@shared/global-loading/GlobalLoading";
import ErrorPage from "../error/ErrorPage";
import { commaizeNumber } from "@toss/utils";
import useCheckKakaoIdAndLicenseStatus from "@/common/hooks/useCheckKakaoIdAndLicenseStatus";

type OrderDetailPageProps = any;
const OrderDetailPage: FC<OrderDetailPageProps> = () => {
  const params = useParams();
  const orderId = params["order_id"]!;
  const navigate = useNavigate();
  const getOrderDetailQuery = useGetOrderDetail(orderId);
  const getMyAccountQuery = useCheckKakaoIdAndLicenseStatus();

  if (getMyAccountQuery.isLoading) {
    return <GlobalLoading visible={true} />;
  }

  if (getMyAccountQuery.isError) {
    // one of exist error
    const error = [
      getMyAccountQuery,
    ].find((query) => query.isError)?.error;
    return <ErrorPage error={error} />;
  }

  if (getOrderDetailQuery.isLoading) {
    return <GlobalLoading visible={true} />;
  }

  if (getOrderDetailQuery.isError) {
    return <ErrorPage />;
  }

  const data = getOrderDetailQuery.data?.data ?? null;

  if (!data) {
    return <div>주문이 존재하지 않습니다.</div>;
  }

  console.log("data", data);

  const {
    order_date,
    customer_name,
    order_number,
    country_code_billing,
    item_list,
  } = data;

  const renderProduct = (item: OrderItem) => {
    console.log(item);
    const sizeTermNameList = item.size_term_list
      .map((term) => term.name)
      .join(", ");
    const colourTermNameList = item.colour_term_list
      .map((term) => term.name)
      .join(", ");
    return (
      <List header="상품 정보" key={item.sku}>
        <List.Item extra={item.product_name}>상품명</List.Item>
        <List.Item extra={commaizeNumber(item.subtotal)}>금액 (수량 반영)</List.Item>
        <List.Item extra={item.product_category_term.name}>카테고리</List.Item>
        <List.Item extra={colourTermNameList}>색상</List.Item>
        <List.Item extra={sizeTermNameList}>사이즈</List.Item>
        <List.Item extra={item.quantity}>수량</List.Item>
      </List>
    );
  };

  return (
    <div className="order-item-detail-page">
      <NavBar onBack={() => navigate(-1)}>주문 상세</NavBar>
      <NoticeBar
        content="상품의 가격은 등록 가격이 아닌 고객이 보는 가격으로 표출됩니다"
        color="alert"
      />
      <List header="주문 정보">
        <List.Item extra={order_number}>주문번호</List.Item>
        <List.Item extra={order_date}>주문일자</List.Item>
        <List.Item extra={country_code_billing}>주문국가</List.Item>
        <List.Item extra={customer_name}>주문자명</List.Item>
      </List>
      {item_list.map((item) => renderProduct(item))}
    </div>
  );
};

export default OrderDetailPage;
