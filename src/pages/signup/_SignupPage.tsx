import { useState, type FC, useEffect } from "react";
import { Controller, useForm, useFormContext } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import useGetSeasonTermList from "@/common/api/api-get-season-term-list";
import useGetStyleTermList from "@/common/api/api-get-style-term-list";
import CustomFormItem from "@/components/@shared/custom-form-item/CustomFormItem";
import { Button, Input, List, NavBar, NoticeBar } from "antd-mobile";
import { Term } from "@/common/types";
import { Upload, UploadFile, message, Button as AntdButton } from "antd";
import { FIELD_NAME } from "@/constants";
import { FormProvider } from "react-hook-form";
import TermSelectFormItem from "@/components/@shared/term-select-form-item/TermSelectFormItem";
import _, { isArray } from "underscore";
import GlobalLoading from "@/components/@shared/global-loading/GlobalLoading";
import useGetBuildingTermList from "@/common/api/api-get-building-term-list";
import { postSignup } from "./signup-api";
import KakaoFloatChatButton from "@/components/@shared/kakao-float-chat-button/KakaoFloatChatButton";
import useGetMadeInTermList from "@/common/api/api-get-made-in-term-list";
import ErrorPage from "../error/ErrorPage";
import { uploadImage as uploadFile } from "@/common/api/api-s3";
import { UploadOutline } from "antd-mobile-icons";

type SignupPageProps = any;
const SignupPage: FC<SignupPageProps> = () => {
  const form = useForm();

  const navigate = useNavigate();
  const [isSigningup, setIsSigningup] = useState(false);
  const [isLicenseUploading, setIsLicenseUploading] =
    useState(false);

  const getBuildingTermListQuery = useGetBuildingTermList();
  const getSeasonTermListQuery = useGetSeasonTermList();
  const getStyleTermListQuery = useGetStyleTermList();
  const getMadeInTermListQuery = useGetMadeInTermList();

  if (
    getBuildingTermListQuery.isLoading ||
    getSeasonTermListQuery.isLoading ||
    getStyleTermListQuery.isLoading ||
    getMadeInTermListQuery.isLoading
  ) {
    return <GlobalLoading visible={true} />;
  }

  if (
    getBuildingTermListQuery.isError ||
    getSeasonTermListQuery.isError ||
    getStyleTermListQuery.isError ||
    getMadeInTermListQuery.isError
  ) {
    // one of exist error
    const error = [
      getSeasonTermListQuery,
      getStyleTermListQuery,
      getMadeInTermListQuery,
    ].find((query) => query.isError)?.error;
    return <ErrorPage error={error} />;
  }

  const buildingTermList = getBuildingTermListQuery.data?.data;
  const seasonTermList = getSeasonTermListQuery.data?.data;
  const styleTermList = getStyleTermListQuery.data?.data;
  const madeInTermList = getMadeInTermListQuery.data?.data;

  if (
    !buildingTermList ||
    !seasonTermList ||
    !styleTermList ||
    !madeInTermList
  ) {
    // @TODO: 에러 메세지 개선
    return <div>필요한 값이 없습니다</div>;
  }

  const onSubmit = (data: any) => {
    // Extracting other properties
    const email = data[FIELD_NAME.EMAIL];
    const password_1 = data[FIELD_NAME.PASSWORD_1];
    const password_2 = data[FIELD_NAME.PASSWORD_2];

    const brand = data[FIELD_NAME.BRAND] ?? null;
    const style_term_id_list = data[FIELD_NAME.STYLE_TERM_ID_LIST] ?? null;
    const season_term_id = data[FIELD_NAME.SEASON_TERM_ID] ?? null;
    const made_in_term_id = data[FIELD_NAME.MADE_IN_TERM_ID] ?? null;

    const building_term_id = data[FIELD_NAME.BUILDING_TERM_ID] ?? null;
    const floor = data[FIELD_NAME.FLOOR] ?? null;
    const room_number = data[FIELD_NAME.ROOM_NUMBER] ?? null;

    const bank = data[FIELD_NAME.BANK] ?? null;
    const account_owner = data[FIELD_NAME.ACCOUNT_OWNER] ?? null;
    const deposit_account = data[FIELD_NAME.DEPOSIT_ACCOUNT] ?? null;
    const phone_number = data[FIELD_NAME.PHONE_NUMBER] ?? null;
    const kakao_id = data[FIELD_NAME.KAKAO_ID] ?? null;
    let license_file_url = data[FIELD_NAME.LICENSE_FILE_URL] ?? null;
    if (isArray(license_file_url)) {
      license_file_url = license_file_url[0];
    }

    const singupData = {
      email,
      password_1,
      password_2,
      brand,
      style_term_id_list,
      season_term_id,
      made_in_term_id,
      building_term_id,
      floor,
      room_number,
      bank,
      account_owner,
      deposit_account,
      phone_number,
      kakao_id,
      license_file_url,
    };

    setIsSigningup(true);

    postSignup(singupData)
      .then(() => {
        message.success("회원가입 신청이 완료되었습니다");
        navigate("/login");
      })
      .catch((err) => {
        if (err && err.response && err.response.data) {
          const errorMessage = err.response.data.message;
          message.error(`회원가입 실패 - ${errorMessage}`);
        } else {
          message.error("회원가입중 알수없는 오류가 발생했습니다");
        }
      })
      .finally(() => {
        setIsSigningup(false);
      });
  };

  return (
    <div className="signup-page">
      <NavBar onBack={() => navigate("/my-account")}>회원가입</NavBar>
      <NoticeBar
        content="회원가입 이후 관리자의 승인이 필요합니다"
        color="alert"
      />
      <FormProvider {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)} className="adm-form">
          <List>
            {/* 이메일 */}
            <EmailFormItem />

            {/* 비밀번호 - 1 */}
            <Password1FormItem />

            {/* 비밀번호 - 2 */}
            <Password2FormItem />

            {/* 브랜드 */}
            <BrandFormItem />

            {/* 스타일 */}
            <StyleFormItem termList={styleTermList} />

            {/* 시즌 */}
            <SeasonFormItem termList={seasonTermList} />

            {/* 제조국 */}
            <MadeInFormItem termList={madeInTermList} />

            {/* 건물 */}
            <BuildingFormItem termList={buildingTermList} />

            {/* 층 */}
            <FloorFormItem />

            {/* 호수 */}
            <RoomNumberFormItem />

            {/* 은행 */}
            <BankFormItem />

            {/* 계좌번호 */}
            <DepositAccountFormItem />

            {/* 예금주 */}
            <AccountOwnerFormItem />

            {/* 매장 폰 번호 */}
            <PhoneNumberFormItem />

            {/* 카카오 아이디 필드 */}
            <KakaoIdFormItem />

            {/* 사업자 등록증 필드 */}
            <LicenseUploadFormItem isUploading={isLicenseUploading} setIsUploading={setIsLicenseUploading} />
          </List>
          <div className="adm-form-footer">
            <Button
              block
              type="submit"
              color="primary"
              size="large"
              loading={isSigningup}
              disabled={isLicenseUploading}
            >
              회원가입
            </Button>
          </div>
        </form>
      </FormProvider>
      <KakaoFloatChatButton />
    </div>
  );
};

export default SignupPage;

const EmailFormItem = () => {
  const form = useFormContext();

  return (
    <Controller
      name={FIELD_NAME.EMAIL}
      control={form.control}
      render={({
        field: { ref, ...field },
        fieldState: { invalid, error },
      }) => {
        return (
          <CustomFormItem
            label="이메일"
            required
            showError={invalid}
            errorMessage={error?.message}
          >
            <Input placeholder="이메일 입력" {...field} ref={ref} />
          </CustomFormItem>
        );
      }}
      rules={{ required: "이메일을 입력해주세요" }}
    />
  );
};

const Password1FormItem = () => {
  const form = useFormContext();

  return (
    <Controller
      name={FIELD_NAME.PASSWORD_1}
      control={form.control}
      render={({
        field: { ref, ...field },
        fieldState: { invalid, error },
      }) => {
        return (
          <CustomFormItem
            label="비밀번호"
            required
            showError={invalid}
            errorMessage={error?.message}
          >
            <Input placeholder="비밀번호 입력" {...field} ref={ref} />
          </CustomFormItem>
        );
      }}
      rules={{ required: "비밀번호를 입력해주세요" }}
    />
  );
};

const Password2FormItem = () => {
  const form = useFormContext();

  return (
    <Controller
      name={FIELD_NAME.PASSWORD_2}
      control={form.control}
      render={({
        field: { ref, ...field },
        fieldState: { invalid, error },
      }) => {
        return (
          <CustomFormItem
            label="비밀번호 재확인"
            required
            showError={invalid}
            errorMessage={error?.message}
          >
            <Input placeholder="비밀번호 입력" {...field} ref={ref} />
          </CustomFormItem>
        );
      }}
      rules={{ required: "비밀번호를 입력해주세요" }}
    />
  );
};

const BrandFormItem = () => {
  const form = useFormContext();

  return (
    <Controller
      name={FIELD_NAME.BRAND}
      control={form.control}
      render={({
        field: { ref, ...field },
        fieldState: { invalid, error },
      }) => {
        return (
          <CustomFormItem
            label="브랜드"
            showError={invalid}
            required
            errorMessage={error?.message}
          >
            <Input placeholder="브랜드 입력" {...field} ref={ref} />
          </CustomFormItem>
        );
      }}
      rules={{ required: "브랜드를 입력해주세요" }}
    />
  );
};

const StyleFormItem = ({ termList }: { termList: Term[] }) => {
  const form = useFormContext();

  return (
    <TermSelectFormItem
      termList={termList}
      isMultiple={true}
      fieldName={FIELD_NAME.STYLE_TERM_ID_LIST}
      label="스타일"
      form={form}
      required={false}
    />
  );
};

const SeasonFormItem = ({ termList }: { termList: Term[] }) => {
  const form = useFormContext();

  return (
    <TermSelectFormItem
      termList={termList}
      isMultiple={false}
      fieldName={FIELD_NAME.SEASON_TERM_ID}
      label="시즌"
      form={form}
      required={false}
    />
  );
};

const MadeInFormItem = ({ termList }: { termList: Term[] }) => {
  const form = useFormContext();

  return (
    <TermSelectFormItem
      termList={termList}
      isMultiple={false}
      fieldName={FIELD_NAME.MADE_IN_TERM_ID}
      label="제조국"
      form={form}
      required={false}
    />
  );
};

const BuildingFormItem = ({ termList }: { termList: Term[] }) => {
  const form = useFormContext();

  return (
    <TermSelectFormItem
      termList={termList}
      isMultiple={false}
      fieldName={FIELD_NAME.BUILDING_TERM_ID}
      label="건물"
      form={form}
      required={true}
      errorMessage="건물을 선택해주세요"
    />
  );
};

const FloorFormItem = () => {
  const form = useFormContext();

  return (
    <Controller
      name={FIELD_NAME.FLOOR}
      control={form.control}
      render={({
        field: { ref, ...field },
        fieldState: { invalid, error },
      }) => {
        return (
          <CustomFormItem
            label="층"
            required
            showError={invalid}
            errorMessage={error?.message}
          >
            <Input placeholder="층 입력" {...field} ref={ref} />
          </CustomFormItem>
        );
      }}
      rules={{ required: "층을 입력해주세요" }}
    />
  );
};

const RoomNumberFormItem = () => {
  const form = useFormContext();

  return (
    <Controller
      name={FIELD_NAME.ROOM_NUMBER}
      control={form.control}
      render={({
        field: { ref, ...field },
        fieldState: { invalid, error },
      }) => {
        return (
          <CustomFormItem
            label="호수"
            required
            showError={invalid}
            errorMessage={error?.message}
          >
            <Input placeholder="호수 입력" {...field} ref={ref} />
          </CustomFormItem>
        );
      }}
      rules={{ required: "호수를 입력해주세요" }}
    />
  );
};

const BankFormItem = () => {
  const form = useFormContext();

  return (
    <Controller
      name={FIELD_NAME.BANK}
      control={form.control}
      render={({
        field: { ref, ...field },
        fieldState: { invalid, error },
      }) => {
        return (
          <CustomFormItem
            label="은행"
            required
            showError={invalid}
            errorMessage={error?.message}
          >
            <Input placeholder="은행 입력" {...field} ref={ref} />
          </CustomFormItem>
        );
      }}
      rules={{ required: "은행 입력해주세요" }}
    />
  );
};

const DepositAccountFormItem = () => {
  const form = useFormContext();

  return (
    <Controller
      name={FIELD_NAME.DEPOSIT_ACCOUNT}
      control={form.control}
      render={({
        field: { ref, ...field },
        fieldState: { invalid, error },
      }) => {
        return (
          <CustomFormItem
            label="계좌번호"
            showError={invalid}
            required
            errorMessage={error?.message}
          >
            <Input placeholder="계좌번호 입력" {...field} ref={ref} />
          </CustomFormItem>
        );
      }}
      rules={{ required: "계좌번호를 입력해주세요" }}
    />
  );
};

const AccountOwnerFormItem = () => {
  const form = useFormContext();

  return (
    <Controller
      name={FIELD_NAME.ACCOUNT_OWNER}
      control={form.control}
      render={({
        field: { ref, ...field },
        fieldState: { invalid, error },
      }) => {
        return (
          <CustomFormItem
            label="예금주명"
            showError={invalid}
            required
            errorMessage={error?.message}
          >
            <Input placeholder="예금주명 입력" {...field} ref={ref} />
          </CustomFormItem>
        );
      }}
      rules={{ required: "예금주명을 입력해주세요" }}
    />
  );
};

const PhoneNumberFormItem = () => {
  const form = useFormContext();

  return (
    <Controller
      name={FIELD_NAME.PHONE_NUMBER}
      control={form.control}
      render={({
        field: { ref, ...field },
        fieldState: { invalid, error },
      }) => {
        return (
          <CustomFormItem
            label="매장 폰 번호"
            showError={invalid}
            required
            errorMessage={error?.message}
          >
            <Input placeholder="매장 폰 번호 입력" {...field} ref={ref} />
          </CustomFormItem>
        );
      }}
      rules={{ required: "매장 폰 번호를 입력해주세요" }}
    />
  );
};

const KakaoIdFormItem = () => {
  const form = useFormContext();

  return (
    <Controller
      name={FIELD_NAME.KAKAO_ID}
      control={form.control}
      render={({
        field: { ref, ...field },
        fieldState: { invalid, error },
      }) => {
        return (
          <CustomFormItem
            label="카카오톡 아이디"
            showError={invalid}
            required
            errorMessage={error?.message}
          >
            <Input placeholder="카카오톡 아이디 입력" {...field} ref={ref} />
          </CustomFormItem>
        );
      }}
      rules={{ required: "카카오톡 아이디를 입력해주세요" }}
    />
  );
};

const LicenseUploadFormItem = ({
  isUploading,
  setIsUploading,
}: {
  isUploading: boolean;
  setIsUploading: React.Dispatch<React.SetStateAction<boolean>>;
}) => {
  const form = useFormContext();
  const [selected, setSelected] = useState<Array<UploadFile>>([]);

  useEffect(() => {
    form.setValue(
      FIELD_NAME.LICENSE_FILE_URL,
      selected.map((item) => item.url)
    );
    form.clearErrors(FIELD_NAME.LICENSE_FILE_URL);
  }, [selected]);

  async function handleUpload(file: any) {
    try {
      const fileUrl = await uploadFile(file);
      return {
        fileUrl: fileUrl.imageUrl,
        fileKey: fileUrl.imageUrl,
        fileName: file.name,
      };
    } catch (err) {
      message.error("업로드에 실패했습니다. 잠시 후 다시 시도해주세요!");
      throw err;
    }
  }

  return (
    <Controller
      name={FIELD_NAME.LICENSE_FILE_URL}
      control={form.control}
      render={({ fieldState: { invalid, error } }) => (
        <CustomFormItem
          label="사업자 등록증"
          required
          showError={invalid}
          errorMessage={error?.message}
        >
          <Upload
            beforeUpload={(file) => {
              return new Promise((resolve, reject) => {
                // file size limit is 5mb
                if (file.size > 5 * 1024 * 1024) {
                  message.error("5MB 이하만 업로드 가능합니다");
                  return reject();
                }
                resolve();
              });
            }}
            customRequest={({ file }) => {
              setIsUploading(true);
              handleUpload(file)
                .then(({ fileUrl, fileKey, fileName }) => {
                  const uploadFile: UploadFile = {
                    uid: fileKey,
                    name: fileName,
                    url: fileUrl,
                    status: "done",
                  };
                  setSelected((prev) => [...prev, uploadFile]);
                })
                .catch(() => {
                  alert(
                    `업로드중 오류가 발생했습니다 - 파일명: ${
                      (file as any).name
                    }`
                  );
                  window.location.reload();
                })
                .finally(() => {
                  setIsUploading(false);
                });
            }}
            fileList={selected}
            maxCount={1}
            multiple={false}
            accept="*/*"
            onChange={({ fileList }) => {
              setSelected(fileList.filter((file) => file.status === "done"));
            }}
            disabled={isUploading}
          >
            <AntdButton
              icon={<UploadOutline />}
              loading={isUploading}
              disabled={isUploading}
            >
              업로드
            </AntdButton>
          </Upload>
        </CustomFormItem>
      )}
      rules={{ required: "사업자 등록증을 선택해주세요" }}
    />
  );
};
