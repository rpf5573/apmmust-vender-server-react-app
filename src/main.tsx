import ReactDOM from "react-dom/client";
import { ReactQueryDevtools } from "@tanstack/react-query-devtools";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import "./common/scss/style.scss";
import { ConfigProvider } from "antd-mobile";
import koKR from "antd-mobile/es/locales/ko-KR";

// Error Monitoring
import Bugsnag from '@bugsnag/js';
import BugsnagPluginReact from '@bugsnag/plugin-react';


import HomePage from "@pages/home/_HomePage.tsx";
import LoginPage from "@pages/login/_LoginPage.tsx";
import SignupPage from "@pages/signup/_SignupPage.tsx";
import { ErrorBoundary } from "react-error-boundary";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import GlobalError from "@components/@shared/global-error/GlobalError";
import { Toaster } from "react-hot-toast";
import { message } from "antd";

const router = createBrowserRouter([
  {
    path: "/*",
    element: <HomePage />,
  },
  {
    path: "/login",
    element: <LoginPage />,
  },
  {
    path: "/signup",
    element: <SignupPage />,
  },
]);

message.config({
  maxCount: 1,
});

// Create a client
const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      retry: 3,
      suspense: false,
      refetchOnWindowFocus: false, // default: true
      retryDelay: 200,
    },
  },
});

// Start error monitoring before redering
Bugsnag.start({
  apiKey: '7c3327995337a2275a1bbb5891925af3',
  plugins: [new BugsnagPluginReact()]
});

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <ErrorBoundary FallbackComponent={GlobalError}>
    <ConfigProvider locale={koKR}>
      <QueryClientProvider client={queryClient}>
        <RouterProvider router={router} />
        <Toaster />
        <ReactQueryDevtools initialIsOpen={false} />
      </QueryClientProvider>
    </ConfigProvider>
  </ErrorBoundary>
);
