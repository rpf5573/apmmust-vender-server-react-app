import axiosInstance from "@/axiosInstance";
import { API, Notice } from "@/common/types";
import { useQuery } from "@tanstack/react-query";

type ApiGetNoticeDetail = API<{ notice_id: string }, Notice>;

export const getNoticeDetail = async (noticeId: string) => {
  const response = await axiosInstance.get<ApiGetNoticeDetail["response"]>(
    `${
      import.meta.env.VITE_API_URL
    }/notice/get-notice-detail?notice_id=${noticeId}`,
    {
      timeout: 3000,
    }
  );

  return response.data;
};

export const useGetNoticeDetail = (noticeId: string) => {
  return useQuery<ApiGetNoticeDetail["response"]>({
    queryKey: ["get-notice-detail", noticeId],
    queryFn: () => getNoticeDetail(noticeId),
    staleTime: 1000 * 60 * 60 * 24, // 24시간 동안 캐싱
    gcTime: 1000 * 60 * 60 * 24, // 24시간 동안 캐싱
  });
};
