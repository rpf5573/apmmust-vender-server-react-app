import TokenContoller from "@/TokenController";
import KakaoFloatChatButton from "@/components/@shared/kakao-float-chat-button/KakaoFloatChatButton";
import { Result } from "antd";
import { Button } from "antd-mobile";
import { ExceptionStatusType } from "antd/es/result";
import { AxiosError } from "axios";
import { useEffect } from "react";

import Bugsnag from '@bugsnag/js';

type ErrorPageProps = {
  error?: Error | AxiosError | undefined | null;
};
const ErrorPage = ({ error }: ErrorPageProps) => {
  console.error(error);

  const handleLogoutButtonClick = () => {
    TokenContoller.clear();
    window.location.href = "/";
  };

  let status: ExceptionStatusType = 500;
  let subTitle = "알수없는 오류가 발생했습니다";

  if (error && error instanceof Error) {
    subTitle = error.message;
  }

  if (error && error instanceof AxiosError) {
    status = (error.response?.status as  ExceptionStatusType) || 500;
    console.log('status', status);
    if ((status as number) === 401 || (status as number) === 403) {
      subTitle = "로그인이 필요합니다";
      status = 403;
    } else {
      subTitle = error.response?.data.message || "알수없는 오류가 발생했습니다";
    }
  }

  if (status !== '403' && status !== '404' && status !== '500') {
    status = 500;
  }

  useEffect(() => {
    // AxiosError는 axios단에서 처리를 해주기 때문에 여기서 처리하지 않는다
    error && !(error instanceof AxiosError) && Bugsnag.notify(error);
  }, [error]);

  console.log('before rendering Result Error View');

  return (
    <Result
      status={status}
      title={status}
      subTitle={subTitle}
      extra={
        <div>
          <Button color="primary" onClick={handleLogoutButtonClick}>
            홈으로 돌아가기
          </Button>
          <KakaoFloatChatButton />
        </div>
      }
    />
  );
};
export default ErrorPage;
