import kakaoButtonImage from "@/common/assets/kakao-button.png";

const KakaoFloatChatButton = () => {
  return (
    <div
      id="plusfriend-chat-button"
      className="fixed right-1 bottom-12 !bg-transparent"
    >
      <a href="http://pf.kakao.com/_ZxkVZG/chat" target="_blank">
        <img
          src={kakaoButtonImage}
          title="플러스친구 1:1 채팅 버튼"
          alt="플러스친구 1:1 채팅 버튼"
          style={{ width: "50px" }}
        />
      </a>
    </div>
  );
};

export default KakaoFloatChatButton;
