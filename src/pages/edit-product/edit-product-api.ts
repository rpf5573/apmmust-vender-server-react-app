import axiosInstance from "@/axiosInstance";
import { API } from "@/common/types";
import { AxiosResponse } from "axios";

export type ApiEditProduct = API<
  {
    product_id: string;
    product_korean_name: string;
    product_english_name: string;
    regular_price: number;
    sale_price: number;
    weight: number;
    product_category_term_id: string;
    style_term_id_list: Array<number>;
    season_term_id: string;
    featured_image_url: string;
    gallery_image_url_list: Array<string>;
    made_in_term_id: string;
    composition_list: Record<string, string | number>;
  },
  {
    code: string;
    data: undefined;
    message: string;
  }
>;

// delete product
export const editProduct = async (product: ApiEditProduct["params"]) => {
  const url = `${import.meta.env.VITE_API_URL}/product/edit-product`;
  const response = await axiosInstance.patch<
    ApiEditProduct["response"],
    AxiosResponse<ApiEditProduct["response"]>
  >(url, product, {
    timeout: 60000,
  });

  return response.data;
};
