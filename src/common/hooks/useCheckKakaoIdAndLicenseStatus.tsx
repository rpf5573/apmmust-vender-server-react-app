import useGetMyAccount from '@/pages/my-account/my-account-api';
import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

const useCheckKakaoIdAndLicenseStatus = () => {
  console.log('useCheckKakaoIdAndLicenseStatus is called');
  const getMyAccountQuery = useGetMyAccount();
  const navigate = useNavigate();

  useEffect(() => {
    if (getMyAccountQuery.isLoading || getMyAccountQuery.isError) {
      return;
    }

    const myAccount = getMyAccountQuery.data?.data ?? null;

    if (myAccount) {
      if (!myAccount.kakao_id && !myAccount.license_file_url) {
        alert("카카오 아이디와 사업자 등록증을 등록해주세요.");
        navigate("/my-account");
        return;
      }

      if (!myAccount.kakao_id) {
        alert("카카오 아이디를 등록해주세요.");
        navigate("/my-account");
        return;
      }
      if (!myAccount.license_file_url) {
        alert("사업자 등록증을 등록해주세요.");
        navigate("/my-account");
        return;
      }
    }
  }, [getMyAccountQuery]);

  return getMyAccountQuery;
};

export default useCheckKakaoIdAndLicenseStatus;
