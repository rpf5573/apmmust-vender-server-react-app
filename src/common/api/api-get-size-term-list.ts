import axiosInstance from "@/axiosInstance";
import { API, Term } from "../types";
import { AxiosResponse } from "axios";
import { useQuery } from "@tanstack/react-query";

export type ApiGetSizeTermList = API<unknown, Term[]>;

export const getSizeTermList = async () => {
  const url = `${import.meta.env.VITE_API_URL}/term/get-size-term-list`;
  const response = await axiosInstance.get<
    ApiGetSizeTermList["response"],
    AxiosResponse<ApiGetSizeTermList["response"]>
  >(url, {
    timeout: 1000 * 3,
  });

  return response.data;
};

export const useGetSizeTermList = () =>
  useQuery<ApiGetSizeTermList["response"]>({
    queryKey: ["get-size-term-list"],
    queryFn: getSizeTermList,
    staleTime: 1000 * 60 * 60 * 24, // 24시간 동안 캐싱
    gcTime: 1000 * 60 * 60 * 24, // 24시간 동안 캐싱
  });

export default useGetSizeTermList;
