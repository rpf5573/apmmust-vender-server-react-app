import axiosInstance from "@/axiosInstance";
import { API } from "@/common/types";
import { AxiosResponse } from "axios";

export type ApiAddProduct = API<
  {
    product_korean_name: string;
    product_english_name: string;
    regular_price: number;
    sale_price: number;
    weight: number;
    size_term_id_list: number[];
    colour_term_id_list: number[];
    product_category_term_id: number;
    style_term_id_list: number[];
    season_term_id: number;
    featured_image_url: string;
    gallery_image_url_list: string[];
    made_in_term_id: number;
    composition_list: Record<string, string | number>;
  },
  {
    code: string;
    data: undefined;
    message: string;
  }
>;

// delete product
export const addProduct = async (product: ApiAddProduct["params"]) => {
  const url = `${import.meta.env.VITE_API_URL}/product/add-product`;
  const response = await axiosInstance.post<
    ApiAddProduct["response"],
    AxiosResponse<ApiAddProduct["response"]>
  >(url, product, {
    timeout: 60000,
  });

  return response.data;
};
