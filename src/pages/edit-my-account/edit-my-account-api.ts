import axiosInstance from "@/axiosInstance";
import { API } from "@/common/types";
import { AxiosResponse } from "axios";

export type ApiEditMyAccount = API<
  {
    password: string;
    bank: string;
    deposit_account: string;
    phone_number: string;
    style_term_id_list: Array<string>;
    season_term_id: string;
    kakao_id: string;
    license_file_url: string;
  },
  {
    code: string;
    data: undefined;
    message: string;
  }
>;

// delete product
export const editMyAccount = async (myAccount: ApiEditMyAccount["params"]) => {
  const url = `${import.meta.env.VITE_API_URL}/my-account/edit-my-account`;
  const response = await axiosInstance.patch<
    ApiEditMyAccount["params"],
    AxiosResponse<ApiEditMyAccount["response"]>
  >(url, myAccount, {
    timeout: 30000,
  });

  return response.data;
};
